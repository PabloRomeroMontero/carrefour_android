package com.rpv.carrefour.data.local

import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.*

interface Repository {

    //TipoApp
    fun insertAppVersion(appVersion: TipoApp): Long
    fun getAppVersion(): LiveData<List<TipoApp>>
    fun deleteAppVersion()


    //TipoUsuario
    fun insertUser(usuario: TipoUsuario): Long
    fun getUsers(): LiveData<List<TipoUsuario>>
    fun getUser(idUser: Int): TipoUsuario
    fun getLastUser(): TipoUsuario
    fun getSizeUserList(): Int
    fun deleteUsuarios()


    //TipoTipo
    fun insertTipoTipo(tipoTipo: TipoTipo): Long
    fun getTipoList(): LiveData<List<TipoTipo>>
    fun deleteTipoTipo()
    fun getTipoByCodTipo(idTipoTipo: String, idTipo: String): LiveData<List<TipoTipo>>


    //TipoCentroHoy
    fun insertTipoCentroHoy(tipoCentroHoy: TipoCentroHoy): Long
    fun getAllCentrosList(): LiveData<List<TipoCentroHoy>>
    fun getCentrHoyoNotToday(hoy: Boolean): LiveData<List<TipoCentroHoy>>
    fun deleteTipoCentrosHoy()
    fun updateCentroHoy(tipoCentroHoy: TipoCentroHoy): Int
    fun getCentroById(idCentro: String): TipoCentroHoy



    //TipoVisita
    fun insertTipoVisita(tipoVisita: TipoVisita): Long
    fun getVisitaList(): LiveData<List<TipoVisita>>
    fun deleteTipoVisita()
    fun getVisitaById(idVisita: String): TipoVisita
    fun updateTipoVisita(tipoVisita: TipoVisita): Int



    //TipoRespuestaPregunta
    fun insertTipoRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Long
    fun getRespuestaPreguntaList(): LiveData<List<TipoRespuestaPregunta>>
    fun deleteRespuestaPregunta()
    fun getRespuestaPreguntaById(idRespuestaPregunta: String): TipoRespuestaPregunta
    fun updateRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Int


    //TipoHoraNomina
    fun insertReporteHoraNomina(reporteHoraNomina: TipoReporteHoraNomina): Long
    fun getReporteHoraNominaList() :LiveData<List<TipoReporteHoraNomina>>
    fun deleteReporteHoraNomina()
    fun getHoraNominaById(idHoraNomina: String): List<TipoHoraNomina>



    //TipoReporteHoraNomina
    fun insertHoraNomina(HoraNomina: TipoHoraNomina): Long
    fun getHoraNominaList() : LiveData<List<TipoHoraNomina>>
    fun deleteHoraNomina()
}