package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.TipoHoraNomina
import com.rpv.carrefour.tipos.TipoReporteHoraNomina


@Dao
interface TipoHoraNominaDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertHoraNomina(HoraNomina: TipoHoraNomina): Long

    @Query("SELECT * FROM TipoHoraNomina")
    fun getHoraNominaList() : LiveData<List<TipoHoraNomina>>

    @Query("SELECT * FROM TipoHoraNomina WHERE IdTipoHora = :idHoraNomina")
    fun getHoraNominaById(idHoraNomina: String): List<TipoHoraNomina>


    @Query("DELETE FROM TipoHoraNomina")
    fun deleteHoraNomina()
}