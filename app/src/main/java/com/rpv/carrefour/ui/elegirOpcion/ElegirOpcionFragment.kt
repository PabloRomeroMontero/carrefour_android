package com.rpv.carrefour.ui.elegirOpcion

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rpv.carrefour.BuildConfig
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.TipoPeticion
import com.rpv.carrefour.tipos.TipoRespuesta
import com.rpv.carrefour.tipos.TipoTipo
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import com.rpv.carrefour.utils.*
import es.alesagal.recycleradapter.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_elegir_opcion.*
import kotlinx.android.synthetic.main.item_opcion_recyclerview_login.view.*


class ElegirOpcionFragment : Fragment(R.layout.fragment_elegir_opcion) {
    private val args: ElegirOpcionFragmentArgs by navArgs()

    private var listaFunciones: MutableList<TipoTipo> = mutableListOf()
    private var listaTipos: List<TipoTipo> = listOf()
    private var listaOpciones: List<Int> = listOf()
    private val navController by lazy {
        findNavController()
    }

    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }


    private fun setupViews() {
        setColorWindows(requireActivity())
        viewModel.getTipoList().observe(this@ElegirOpcionFragment.viewLifecycleOwner) { lista ->
            //viewModel.refreshListTipos()
            listaTipos = lista
        }
        viewModel.getListaOpciones().observe(this@ElegirOpcionFragment.viewLifecycleOwner) { lista ->
            listaOpciones = lista
            if(listaOpciones.isNotEmpty()){
                setupRcCriba()
            }
        }
        getFunciones()
    }

    private fun setupRc(mList: List<TipoTipo>) {
        val mAdapter = object : RecyclerAdapter<ViewHolderOpc>(
            mList,
            R.layout.item_opcion_recyclerview_login,
            ViewHolderOpc::class.java
        ) {

            override fun onBindViewHolder(holder: ViewHolderOpc, position: Int) {
                val item = mList[position]

                holder.lblItem.text = item.descTipo

                holder.clItem.setOnClickListener { navToOption(item) }
            }
        }
        rclOpcion.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

    }

    private fun navToOption(item: TipoTipo) {
        when (item.codTipo) {
            0 -> {
                //HacerPreguntas
                var a = args.userId
                this.navController.navigate(
                    ElegirOpcionFragmentDirections.actionElegirOpcionFragmentToElegirCentroFragment(
                        args.userId
                    )
                )
            }
            1 -> {
                //Asignacion Usuarios
            }
            2 -> {
                //Horas Nomina
                navController.navigate(
                    ElegirOpcionFragmentDirections.actionElegirOpcionFragmentDestinationToHorasNominasFragmentDestination()
                )

            }
        }


    }

    //region [LLAMADAS]
    private fun getFunciones() {

            val petition = TipoPeticion(
                WS.Get.FUNCIONES_BY_ID,
                args.perfilId.toString(),
                BuildConfig.VERSION_CODE
            )
            WS(activity, petition, object : WS.Callback {
                override fun onCompleted(respuesta: TipoRespuesta) {
                    val dec: String
                    try {
                        dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                        viewModel.addlistaOpciones(dec)

                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DECRYPT)
                        return
                    }
                }


                override fun onError(respuesta: TipoRespuesta) {
                    dialog(respuesta.estado)
                }

                override fun onDenied(respuesta: TipoRespuesta) {
                    dialog(respuesta.respuesta)
                }

                override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                    dialog(respuesta.respuesta)
                }

            }, true)
        }

//endregion

    fun setupRcCriba() {
        listaFunciones.clear()
        for (tipo in listaTipos) {
            for (funciones in listaOpciones) {
                if (tipo.codTipo == funciones && tipo.idTipo == C.TIPO.TIPO_FUNCION && funciones == 0) {
                    listaFunciones.add(tipo)
                }
            }
        }
        setupRc(listaFunciones)
    }
}

class ViewHolderOpc(view: View) : RecyclerView.ViewHolder(view) {
    var lblItem: TextView = view.lblItemOpcionLoginFragment
    var clItem: View = view.clOpcionItem
}