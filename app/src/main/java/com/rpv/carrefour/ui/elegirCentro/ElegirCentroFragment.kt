package com.rpv.carrefour.ui.elegirCentro

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.BuildConfig
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.*
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import com.rpv.carrefour.utils.*
import com.rpv.carrefour.utils.lottieAlertDialog.*
import es.alesagal.recycleradapter.RecyclerAdapter
import kotlinx.android.synthetic.main.dialog_search_centro.view.*
import kotlinx.android.synthetic.main.fragment_elegir_centro.*
import kotlinx.android.synthetic.main.item_buscar_centros.view.*
import kotlinx.android.synthetic.main.item_centros_recyclerview_login.view.*
import java.util.*

class ElegirCentroFragment : Fragment(R.layout.fragment_elegir_centro) {
    private val args: ElegirCentroFragmentArgs by navArgs()
    private val navController by lazy {
        findNavController()
    }
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }
    private var listaCentrosHoy: List<TipoCentroHoy> = listOf()
    private var listaCentrosNoHoy: List<TipoCentroHoy> = listOf()
    private lateinit var visita: TipoVisita

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupViews() {
        setColorWindows(requireActivity())
        viewModel.getCentroHoyToday().observe(this.viewLifecycleOwner) {
            listaCentrosHoy = it
            setupRcCentro(listaCentrosHoy)
        }

        swipeToRefreshCentro.setOnRefreshListener {
            refreshCentros()
            swipeToRefreshCentro.isRefreshing = false
        }
        btnSearchCenter.setOnClickListener {
            buscarCentroDialog()
        }

    }


    //region    [RC DIALOG CENTROS]
    private fun buscarCentroDialogLottie() {
        val inflater = this.layoutInflater
        val dialogBuilder =
            LottieAlertDialogSearchCenter.Builder(requireActivity(), DialogTypes.TYPE_QUESTION)
                .apply {
                    setCancellListener(object : ClickListenerSearchCenter {
                        override fun onClick(dialog: LottieAlertDialogSearchCenter) {
                            dialog.dismiss()
                        }
                    })
                    setRecyclerViewList(listaCentrosNoHoy)
                    setItemListener(object : ClickListenerSearchCenterItem{
                        override fun onClick(dialog: LottieAlertDialogSearchCenter, item: TipoCentroHoy) {
                            selectItem(item)
                        }
                    })
                    setOnTextChangedListener(object : TextWatcher{
                        override fun onTextChanged(
                            dialog: LottieAlertDialogSearchCenter,
                            s: CharSequence?,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {
                            dialog.setList(viewModel.getCenterByFilter(s.toString()))
                        }
                    })
                }


        val lottieAlertDialog = dialogBuilder.build()
        lottieAlertDialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        lottieAlertDialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        lottieAlertDialog.show()
        viewModel.getCentroHoyNotToday().observe(this.viewLifecycleOwner) {
            listaCentrosNoHoy = it
            lottieAlertDialog.setList(listaCentrosNoHoy)
        }
    }

    private fun buscarCentroDialog() {
        val inflater = this.layoutInflater
        val dialogBuilder = AlertDialog.Builder(requireActivity())
        val dialogView: View = inflater.inflate(R.layout.dialog_search_centro, null)

        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        val editText = dialogView.findViewById<EditText>(R.id.editTextSearchCenter)
        val btnCancell: Button = dialogView.findViewById(R.id.btnCancelar_dialogSearch)

        viewModel.getCentroHoyNotToday().observe(this.viewLifecycleOwner) {
            listaCentrosNoHoy = it
            setupRcSearchCentroDialog(listaCentrosNoHoy, dialogView)
        }
        setupRcSearchCentroDialog(listaCentrosNoHoy, dialogView)
        editText!!.addTextChangedListener(object : android.text.TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                setupRcSearchCentroDialog(viewModel.getCenterByFilter(s.toString()), dialogView)
            }

            override fun afterTextChanged(s: Editable?) {}

        })
        btnCancell.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()


    }

    private fun setupRcSearchCentroDialog(mList: List<TipoCentroHoy>, dialogView: View) {
        val mAdapter = object : RecyclerAdapter<ViewHolderSearchCent>(
            mList,
            R.layout.item_buscar_centros,
            ViewHolderSearchCent::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderSearchCent, position: Int) {
                val item = mList[position]
                holder.lblItemNombre.text = item.nombre
                holder.lblItemId.text = item.idCentro.toString()
                holder.clItem.setOnClickListener {
                    selectItem(item)

                }
            }
        }
        dialogView.rcCentrosSearch.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }

    fun selectItem(item: TipoCentroHoy) {
        val alertDialog =
            LottieAlertDialog.Builder(context, DialogTypes.TYPE_QUESTION).apply {
                setTitle("Añadir Centro")
                    .setDescription("¿Desea añadir este centro a su ruta de hoy?")
                    .setPositiveText("Añadir")
                    .setNegativeText("Cancelar")
                    .setPositiveButtonColor(R.color.azulOscuro)
                    .setPositiveTextColor(R.color.blanco)
                    .setNegativeButtonColor(R.color.red_toolbar)
                    .setNegativeTextColor(R.color.blanco)
                    // Error View
                    .setPositiveListener(object : ClickListener {
                        override fun onClick(dialog: LottieAlertDialog) {
                            item.hoy = true
                            viewModel.updateCenterHoy(item)
                            dialog.changeDialog(
                                LottieAlertDialog.Builder(
                                    getContext(),
                                    DialogTypes.TYPE_SUCCESS
                                )
                                    .setTitle("")
                                    .setDescription("Se ha añadido el centro correctamente.")
                                    .setPositiveText("Continuar")
                                    .setPositiveListener(object : ClickListener {
                                        override fun onClick(dialog: LottieAlertDialog) {
                                            dialog.dismiss()
                                        }
                                    })
                            )
                        }
                    })
                    .setNegativeListener(object : ClickListener {
                        override fun onClick(dialog: LottieAlertDialog) {
                            dialog.dismiss()
                        }
                    })
            }
        alertDialog.build().show()
    }
    //endregion

    //region    [RC CENTROS]

    private fun setupRcCentro(mList: List<TipoCentroHoy>) {
        val mAdapter = object : RecyclerAdapter<ViewHolderCent>(
            mList,
            R.layout.item_centros_recyclerview_login,
            ViewHolderCent::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderCent, position: Int) {
                val item = mList[position]
                holder.lblItem.text = spannable {
                    bold(item.nombre + "\n") + normal("Id: " + item.idCentro.toString())
                }
                holder.clItem.setOnClickListener {
                    navigateTo(item)
                }
            }
        }
        rclCentro.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }


    fun navigateTo(item: TipoCentroHoy) {
        //HacerPreguntas
        navController.navigate(
            ElegirCentroFragmentDirections.actionElegirCentroFragmentDestinationToPreguntasFragment(
                item.idCentro.toString(), item.idUsuario, "8"
            )
        )
    }

    fun refreshCentros() {
//        la diferencia que tiene este metodo con el getCentros del apartado cuando seleciconamos
//        el usuario es que este no elimina nada de la base de datos para asi seguir teniendo
//        los movimientos del usuario
//        viewModel.deleteTipoCentrosHoy()
        val petitionToday = TipoPeticion(
            WS.Get.CENTROS_BY_USERID,
            viewModel.getLastUser().IdUsuario.toString(),
            BuildConfig.VERSION_CODE
        )
        val petitionNotToday = TipoPeticion(
            WS.Get.TODOS_CENTROS_BY_USERID,
            viewModel.getLastUser().IdUsuario.toString(),
            BuildConfig.VERSION_CODE
        )

        WS(activity, petitionToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
                if(dec.isNotEmpty()) {
                    lateinit var centros: ArrayList<TipoCentro>
                    val gson = Gson()
                    try {
                        val itemTipoCentro = object : TypeToken<ArrayList<TipoCentro>>() {}.type
                        centros = gson.fromJson<ArrayList<TipoCentro>>(dec, itemTipoCentro)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                    centros.forEach {
                        viewModel.insertTipoCentroHoy(
                            TipoCentroHoy(
                                it.idCentro,
                                it.detalle,
                                it.idUsuario,
                                it.semana,
                                it.dia,
                                it.nombre,
                                it.municipio,
                                it.domicilio,
                                it.provincia,
                                it.cCAA,
                                true
                            )
                        )
                    }
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })

        WS(activity, petitionNotToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
                if(dec.isNotEmpty()) {
                    lateinit var centros: ArrayList<TipoCentro>
                    val gson = Gson()
                    try {
                        val itemTipoCentro = object : TypeToken<ArrayList<TipoCentro>>() {}.type
                        centros = gson.fromJson<ArrayList<TipoCentro>>(dec, itemTipoCentro)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                    centros.forEach {
                        viewModel.insertTipoCentroHoy(
                            TipoCentroHoy(
                                it.idCentro,
                                it.detalle,
                                it.idUsuario,
                                it.semana,
                                it.dia,
                                it.nombre,
                                it.municipio,
                                it.domicilio,
                                it.provincia,
                                it.cCAA,
                                false
                            )
                        )
                    }
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

//endregion

}


class ViewHolderCent(view: View) : RecyclerView.ViewHolder(view) {
    var lblItem: TextView = view.lblItemCentroLoginFragment
    var clItem: View = view.clCentroItem
}


class ViewHolderSearchCent(view: View) : RecyclerView.ViewHolder(view) {
    var lblItemNombre: TextView = view.lblNombre_Centro_searchItem
    var lblItemId: TextView = view.lblId_Centro_SearchItem
    var clItem: ConstraintLayout = view.cl_Item_centros_search
}
