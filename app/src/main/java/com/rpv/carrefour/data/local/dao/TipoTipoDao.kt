package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.TipoApp
import com.rpv.carrefour.tipos.TipoTipo

@Dao
interface TipoTipoDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTipoTipo(tipoTipo: TipoTipo): Long

    @Query("SELECT * FROM TipoTipo")
    fun getTipoList(): LiveData<List<TipoTipo>>


    @Query("SELECT * FROM TipoTipo WHERE idTipo =:idTipo AND codTipo = :codTipoTipo")
    fun getTipoByCodTipo(codTipoTipo: String, idTipo:String): LiveData<List<TipoTipo>>


    @Query("DELETE FROM TipoTipo")
    fun deleteTipoTipo()
}