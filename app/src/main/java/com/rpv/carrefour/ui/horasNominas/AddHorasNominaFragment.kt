package com.rpv.carrefour.ui.horasNominas

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rpv.carrefour.R
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.main.fragment_add_horas_nomina.*


class AddHorasNominaFragment : Fragment(R.layout.fragment_add_horas_nomina) {

    private val navController by lazy {
        findNavController()
    }
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgBtnAdd_AddHoraNomina.setOnClickListener {
            val mDialogView =
                LayoutInflater.from(requireContext())
                    .inflate(R.layout.dialog_hora_nomina_tipo, null)
            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView)
            mBuilder.show()

            //Load Spinners...
//
//            if (viewModel.listSeccionesNomina.isNotEmpty() && listTypeHour[0].isNotEmpty()) {
//                loadSpinners(vm.listSeccionesNomina, mDialogView.spnSeccion_DialogHoraNomina)
//                loadSpinners(listTypeHour, mDialogView.spnHoras_DialogHoraNomina)
//
//                //mDialogView.spnHoras_DialogReporteHoraNomina.setSelection(1)
//
//            } else {
//                toast("Error en la carga de datos. Comuníquelo a Sistemas")
//
//            }
        }
    }
}