package com.rpv.carrefour.tipos

import org.json.JSONObject

class TipoLogin(var login: String = "", var pass: String = "", var app: String = "") {

    fun toJson() = JSONObject().apply {
        put("login", login)
        put("pass", pass)
        put("app", app)
    }.toString()

    override fun toString() = "TipoLogin(login='$login', pass='$pass', app='$app')"

}