package com.rpv.carrefour.tipos

import org.json.JSONObject


class TipoRespuesta(var estado: String = "", var respuesta: String = "") {

    constructor(o: JSONObject) : this(
            estado = o.getString("estado"),
            respuesta = o.getString("respuesta")
    )

    override fun toString() = "TipoRespuesta(estado='$estado', respuesta='$respuesta')"

}
