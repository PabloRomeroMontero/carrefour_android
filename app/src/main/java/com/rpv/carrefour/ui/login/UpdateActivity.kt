package com.rpv.carrefour.ui.login

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.rpv.carrefour.R
import com.rpv.carrefour.utils.C
import com.rpv.carrefour.utils.F
import com.rpv.carrefour.utils.systemService
import com.rpv.carrefour.utils.toUri
import java.io.File


class UpdateActivity : AppCompatActivity() {

    private var apk: String = ""

    private var id: Long = -1

    private val downloadReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {

            val referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)

            if (referenceId == id) {
                install(apk)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_update)

        apk = intent.extras?.getString("apk") ?: C.APP.APK

        download(apk)
    }



    private fun download(name: String) {

        File(C.PATH.ARCHIVOS + File.separator + name).also {
            if (it.exists()) {
                if (it.delete())
                    F.log("UpdateActivity.download", "file deleted")
            }
        }

        systemService<DownloadManager>()?.also {
            val request = DownloadManager.Request(Uri.parse(C.CONSTANT.URL_APPS + name))

            request.setTitle("Descarga")
            request.setDescription("Prueba del servicio Download Manager.")

            request.setVisibleInDownloadsUi(false)
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)

            var name2 = name
            var dirType = File.separator + "rpv" + File.separator + C.APP.NAME + File.separator + "archivos"

            request.setDestinationInExternalPublicDir("", name2)

            //iniciamos la descarga
            id = it.enqueue(request)

            val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            registerReceiver(downloadReceiver, filter)

            val pbDownload = findViewById<ProgressBar>(R.id.pbDownload)
            val tvStatus = findViewById<TextView>(R.id.tvStatus)

            Thread(Runnable {
                var downloading = true

                while (downloading) {

                    val q = DownloadManager.Query()
                    q.setFilterById(id)

                    it.query(q)?.also { cursor ->
                        cursor.moveToFirst()
                        val bytesDownloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                        val bytesTotal = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                        val status = statusMessage(cursor)

                        if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                            downloading = false
                        }

                        val dlProgress = (bytesDownloaded.toDouble() * 100f / bytesTotal.toDouble()).toInt()

                        runOnUiThread {
                            pbDownload.progress = dlProgress
                            tvStatus.text = status
                        }

                        cursor.close()
                    }
                }
            }).start()
        }

    }

    private fun install(name: String?) {
        unregisterReceiver(downloadReceiver)

        Intent(Intent.ACTION_VIEW).also { intent ->
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
            toUri(File(C.PATH.ARCHIVOS + File.separator + name))?.also {
                intent.setDataAndType(it, "application/vnd.android.package-archive")
            }
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(intent)
        }

        finish()
    }

    private fun statusMessage(c: Cursor): String {

        return when (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            DownloadManager.STATUS_FAILED -> "Descarga fallida"
            DownloadManager.STATUS_PAUSED -> "Descarga pausada"
            DownloadManager.STATUS_PENDING -> "Descarga pendiente"
            DownloadManager.STATUS_RUNNING -> "Descargando..."
            DownloadManager.STATUS_SUCCESSFUL -> "¡Descarga completada!"
            else -> ""
        }
    }

}