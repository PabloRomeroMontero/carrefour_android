package com.rpv.carrefour.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.rpv.carrefour.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(R.layout.activity_main){

    private val navController by lazy {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        navHostFragment.navController    }

    private val appBarConfiguration: AppBarConfiguration by lazy {
        AppBarConfiguration(
        setOf(
            R.id.mainFragmentDestination
        ), drawerLayout)
    }

    override fun onCreate(saveInstanceState: Bundle?) {
        super.onCreate(saveInstanceState)

// clear FLAG_TRANSLUCENT_STATUS flag:

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

// finally change the color

// finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        setupViews()
    }

    private fun setupViews() {
        setupNavigationViews()
    }

    private fun setupNavigationViews() {
        setSupportActionBar(toolbarMain)
        toolbarMain.setupWithNavController(navController, appBarConfiguration)
        navigationView.setNavigationItemSelectedListener { navigateToOption(it) }
    }

    private fun navigateToOption(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mnuPruebaHome -> navigateToPedidos()
        }
        drawerLayout.closeDrawer((GravityCompat.START))
        return true
    }

    private fun navigateToPedidos() {
        navController.navigate(R.id.mainFragmentDestination)
    }

    //CERRAR DRAWER LAYOUT CUANDO DAMOS A ONBACKPRESS
    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
