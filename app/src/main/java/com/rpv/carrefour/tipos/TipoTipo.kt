package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys= [ "idTipo", "codTipo" ] )
data class TipoTipo(
    val idTipo: Int,
    val codTipo: Int,
    val descTipo: String,
    val orden: Int
) {
    override fun toString(): String {
        return "TipoTipo(idTipo=$idTipo, codTipo=$codTipo, descTipo='$descTipo', orden=$orden)"
    }
}



