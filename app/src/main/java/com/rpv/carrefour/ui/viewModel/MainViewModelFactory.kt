package com.rpv.carrefour.ui.viewModel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rpv.carrefour.data.local.AppDatabase

class MainViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            val key = "com.carrefour.main.viewModel.MainViewModel"
            return if(hashMapViewModel.containsKey(key)){
                getViewModel(key) as T
            } else {
                addViewModel(key, MainViewModel(AppDatabase.getInstance(context)))
                getViewModel(key) as T
            }
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }

    companion object {
        val hashMapViewModel = HashMap<String, MainViewModel>()
        fun addViewModel(key: String, viewModel: MainViewModel){
            hashMapViewModel[key] = viewModel
        }
        fun getViewModel(key: String): MainViewModel? {
            return hashMapViewModel[key]
        }
    }
}