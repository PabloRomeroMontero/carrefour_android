package com.rpv.carrefour.utils.lottieAlertDialog

interface ClickListenerSearchCenter {
    fun onClick(dialog: LottieAlertDialogSearchCenter)
}