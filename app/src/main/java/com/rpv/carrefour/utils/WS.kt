package com.rpv.carrefour.utils

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.tipos.TipoPeticion
import com.rpv.carrefour.tipos.TipoRespuesta
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE
import java.lang.ref.WeakReference


class WS(context: Context?, tipoPeticion: TipoPeticion, callback: Callback, showDialog: Boolean = true) {

    init {
        Task(context, callback, showDialog).execute(tipoPeticion)
    }

    object Get {
        const val APPVERSION = "getAppVersion"
        const val LOGIN = "getLogin"
        const val PETITION = "getPetition"
        const val TIPOS = "getTipos"
        const val FUNCIONES ="getFunciones"
        const val FUNCIONES_BY_ID ="getFuncionesByIdPerfil"
        const val CENTROS_BY_USERID="getCentrosByUserByVisita"
        const val TODOS_CENTROS_BY_USERID =  "getTodosCentrosByUserByVisita"
        const val PREGUNTAS = "getPreguntas"
        const val RECUERDO_RESPUESTAS = "getRecuerdoPreguntasByCentro"
        const val GetHorasNomina = "getHorasNomina"
        const val GetSeccionesNomina = "getSeccionesNomina"
        const val GetTipoHoraNomina = "getTipoHoraNomina"

    }




    object Set {
        const val SEND_VISITA = "getVisita"
        const val SEND_RESPUESTA = "setRespuestas"
        const val SEND_CLOSE_VISITA = "closeVisita"
        const val SEND_REPORTE_NOMINA = "setReporteHoraNomina"
        const val DELETE_REPORTE_NOMINA = "deleteReporteHoraNomina"

    }

    interface Callback {
        fun onCompleted(respuesta: TipoRespuesta)
        fun onError(respuesta: TipoRespuesta)
        fun onDenied(respuesta: TipoRespuesta)
        fun onObsolete(respuesta: TipoRespuesta)
    }




}

private class Task(context: Context?, val callback: WS.Callback, val showDialog: Boolean = true) : AsyncTask<TipoPeticion, String, TipoRespuesta>() {
    private var progressDialog: MaterialDialog? = null
    private val contextRef: WeakReference<Context?> = WeakReference(context)
    private val context: Context? get() = contextRef.get()

    override fun onPreExecute() {

        context?.also {
            if (showDialog)
                progressDialog = MaterialDialog.Builder(it)
                    .title("Conectando")
                    .content("Comprobando conexión con el servidor...")
                    .progress(true, 0)
                    .cancelable(false)
                    .build()
        }
        progressDialog?.show()
    }

    override fun onProgressUpdate(vararg values: String) {
        progressDialog?.setContent(values[0])
    }

    override fun doInBackground(params: Array<TipoPeticion>): TipoRespuesta {
        return this(params[0])
    }

    override fun onPostExecute(result: TipoRespuesta) {
        progressDialog?.dismiss()

        callback.apply {
            when (result.estado) {
                C.RES.OK -> onCompleted(result)
                C.RES.DENIED -> onDenied(result)
                C.RES.OBSOLETE -> onObsolete(result)

                else -> onError(result)
            }
        }
    }

    private operator fun invoke(peticion: TipoPeticion): TipoRespuesta {
        val gson = Gson()
        var respuesta = TipoRespuesta()
        val methodName = WS.Get.PETITION
        val nameSpace = C.CONSTANT.NAMESPACE
        val soapAction = nameSpace + methodName
        val request = SoapObject(nameSpace, methodName)

       //region [Encriptación y paso parametros]
        val enc: String
        try {
            enc = F.Security.encrypt(gson.toJson(peticion), C.KEY.KEY)
        } catch (e: Exception) {
            F.log("WS.invoke", e)
            respuesta.estado = C.RES.ERROR_ENCRYPT
            return respuesta
        }
        request.addProperty("s", enc)
//        //endregion

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)

//region [Credenciales, imei , header and child (¿Para qué?)]
//        val h = Element().createElement(nameSpace, "UserCredentials")
//
//        val credentialKey = Element().createElement(nameSpace, "key")
//        val key = F.getMD5()
//        credentialKey.addChild(Node.TEXT, key)
//        h.addChild(Node.ELEMENT, credentialKey) //var h -> child num 1
//
//        h.addChild(Node.ELEMENT, credentialIme)//var h -> child num 2
//
//        envelope.headerOut = arrayOfNulls(1)
//        envelope.headerOut[0] = h
//endregion

        envelope.dotNet = true
        envelope.setOutputSoapObject(request)
        val transport = HttpTransportSE(C.CONSTANT.URL)
        transport.debug = true

        //region [LLamada al servidor]
        val res: String
        try {
            transport.call(soapAction, envelope)
            // F.log("WS.invoke", transport.requestDump)
            // F.log("WS.invoke", transport.responseDump)
            res = envelope.response.toString()
        } catch (e: Exception) {
            F.log("WS.invoke", e)
            respuesta.estado = C.RES.ERROR_INTERNET
            return respuesta
        }
        //endregion

        //region [Desencriptado]
        val dec: String
        try {
            dec = F.Security.decrypt(res, C.KEY.KEY)
            Log.d("decDes",dec)
        } catch (e: Exception) {
            F.log("WS.invoke", e)
            respuesta.estado = C.RES.ERROR_DECRYPT
            return respuesta
        }
        //endregion

        //region [Parseo de JSON a Object (TipoRespuesta)]
        try {
            val itemTipoRespuesta = object : TypeToken<TipoRespuesta>() {}.type
            respuesta = gson.fromJson<TipoRespuesta>(dec, itemTipoRespuesta)
            Log.d("respDec", respuesta.toString())
        } catch (e: Exception) {
            F.log("WS.invoke", e)
            respuesta.estado = C.RES.ERROR_DESERIALIZE
            return respuesta
        }
        //endregion

        return respuesta
    }

}
