package com.rpv.carrefour.utils.lottieAlertDialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.TipoCentroHoy
import com.rpv.carrefour.ui.elegirCentro.ViewHolderSearchCent
import com.rpv.carrefour.utils.hideSoftKeyboard
import com.rpv.carrefour.utils.openSoftKeyboard
import es.alesagal.recycleradapter.RecyclerAdapter
import kotlinx.android.synthetic.main.dialog_search_centro.*
import kotlinx.android.synthetic.main.dialog_search_centro_lottie.*
import kotlinx.android.synthetic.main.item_buscar_centros.view.*

class LottieAlertDialogSearchCenter private constructor(
    context: Context,
    type: Int?,
    customAsset: String?,
    var addListener: ClickListenerSearchCenterItem?,
    var cancelListener: ClickListenerSearchCenter?,
    var listaCentrosNoHoy: List<TipoCentroHoy>?,
    var textChangedListener: TextWatcher?,
    var editTextListener : ClickListenerSearchCenter?
    ) : AlertDialog(context)
{
    private var mCustomAsset:String? = customAsset
    private var mContext : Context = context
    private var mType:Int?= type

    private lateinit var btnCancell: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var editText: EditText
    private lateinit var lAnimation : LottieAnimationView
    private var animationFadeIn : Animation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
    private lateinit var animationFadeOut: Animation

    init {
        animationFadeIn.duration = 50
        animationFadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out)
        animationFadeOut.duration = 50
    }


    data class Builder(
        private val context: Context? = null,
        private val type:Int? = null,
        private val customAsset:String? = null
    )
    {
        private var editTextListener: ClickListenerSearchCenter? = null
        private var btnCancell:ClickListenerSearchCenter? = null
        private var btnAdd:ClickListenerSearchCenterItem? = null
        private var listaCentrosNoHoy: List<TipoCentroHoy>? = null
        private var textWatcher: com.rpv.carrefour.utils.lottieAlertDialog.TextWatcher? = null


        fun setEditTextListener(listenerSearchCenter: ClickListenerSearchCenter): Builder = apply {
            this.editTextListener = listenerSearchCenter; return@apply
        }
        fun setItemListener(itemListener:ClickListenerSearchCenterItem?) : Builder = apply { this.btnAdd = itemListener ; return@apply}
        fun setCancellListener(cancellListener:ClickListenerSearchCenter?) : Builder = apply { this.btnCancell = cancellListener ; return@apply}

        fun setRecyclerViewList(lista: List<TipoCentroHoy>?): Builder = apply {
            this.listaCentrosNoHoy = lista; return@apply
        }

        fun setOnTextChangedListener(textWatcherListener: com.rpv.carrefour.utils.lottieAlertDialog.TextWatcher) : Builder = apply {
            this.textWatcher = textWatcherListener ; return@apply}


        fun build() = LottieAlertDialogSearchCenter(context!!,type,customAsset,btnAdd,btnCancell,listaCentrosNoHoy, textWatcher,editTextListener)
        fun getContext() : Context? {return context}
        fun getType() : Int? { return  type}
        fun getCustomAsset() : String? { return customAsset}

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_search_centro_lottie)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        findView()
    }

    private fun findView()
    {
        lAnimation=findViewById(R.id.lAnimation)
        editText = editTextSearchCenter
        btnCancell = btnCancelar_dialogSearch
        recyclerView = rcCentrosSearch
        setView()
    }

    private fun setView()
    {
        // SET VIEW
        lAnimation.startAnimation(animationFadeIn)
        lAnimation.visibility=View.GONE
        rlAnimation.visibility = View.GONE

        if(cancelListener != null){
            btnCancell.setOnClickListener{cancelListener?.onClick(LottieAlertDialogSearchCenter@this)}
        }

        if(listaCentrosNoHoy != null)
            setupRcSearchCentroDialog(listaCentrosNoHoy)

        editText.setOnClickListener{editTextListener?.onClick(LottieAlertDialogSearchCenter@this)}

        editText.setOnFocusChangeListener{  v, hasFocus ->
            if(hasFocus) v.openSoftKeyboard() else v.hideSoftKeyboard()
        }

        editText.addTextChangedListener{textChangedListener?.onTextChanged(LottieAlertDialogSearchCenter@this,editText.text.toString(),0,0,0)}



        // TYPE CONTROL
        if (mType == DialogTypes.TYPE_LOADING)
        {
            lAnimation.setAnimation("loading.json")
            lAnimation.repeatCount=LottieDrawable.INFINITE
        }
        else if (mType == DialogTypes.TYPE_SUCCESS)
        {
            lAnimation.setAnimation("success.json")
            lAnimation.repeatCount=0
        }
        else if (mType == DialogTypes.TYPE_ERROR)
        {
            lAnimation.setAnimation("error.json")
            lAnimation.repeatCount=0
        }
        else if (mType == DialogTypes.TYPE_WARNING)
        {
            lAnimation.setAnimation("warning.json")
            lAnimation.repeatCount=0
        }
        else if (mType == DialogTypes.TYPE_QUESTION)
        {
            lAnimation.setAnimation("question.json")
            lAnimation.repeatCount=LottieDrawable.INFINITE
        }
        else if (mType == DialogTypes.TYPE_CUSTOM)
        {
            lAnimation.setAnimation(mCustomAsset)
            lAnimation.repeatCount=LottieDrawable.INFINITE
        }
        lAnimation.playAnimation()
    }

    private fun setupRcSearchCentroDialog(mList: List<TipoCentroHoy>?) {
        val mAdapter = object : RecyclerAdapter<ViewHolderSearchCent>(
            mList!!,
            R.layout.item_buscar_centros,
            ViewHolderSearchCent::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderSearchCent, position: Int) {
                val item = mList!![position]
                holder.lblItemNombre.text = item.nombre
                holder.lblItemId.text = item.idCentro.toString()
                holder.clItem.setOnClickListener {
                    addListener?.onClick(this@LottieAlertDialogSearchCenter,item)
                }
            }
        }
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }

    fun changeDialog(builder : Builder)
    {
        mContext= builder.getContext()!!
        mType=builder.getType()
        mCustomAsset=builder.getCustomAsset()
        lAnimation.startAnimation(animationFadeOut)
        Handler().postDelayed(Runnable { setView() },50)
    }

    fun setList(lista: List<TipoCentroHoy>) {
        setupRcSearchCentroDialog(lista)
    }
}

