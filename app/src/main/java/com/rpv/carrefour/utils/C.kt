package com.rpv.carrefour.utils

import android.Manifest
import android.os.Environment
import com.rpv.carrefour.BuildConfig
import java.io.File
import java.lang.Boolean


object C {

    object APP {
        const val NAME = "carrefour"
        const val APK = "carrefour.apk"
        const val AUTHORITY = "com.rpv.carrefour.fileProvider"
        val DEBUG = BuildConfig.DEBUG
    }

    object KEY {
        const val KEY= "Recursos1319"
    }

    object CONSTANT {
        const val NAMESPACE = "http://carrefour.rpv.es/"
        const val URL = "http://carrefour.rpv.es/CarrefourWebservice.asmx"
        const val URL_APPS = "http://apps.rpv.es:1234/Apps/"




    }

    object REQUEST {
        const val PERMISSION = 1000
        const val PHOTO = 1001
        const val ALARM_LOCATION = 1002
        const val ALARM_LOCATION_CONFIGURATION = 1003
    }

    object ACTION {
        const val LOCATION_CONFIGURATION = ".LOCATION_CONFIGURATION"
        const val LOCATION = ".LOCATION"
    }


    object PERMISSIONS {
        val ALL = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.INTERNET)

        val LOCATION = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE)

        /*
        val PHOTO = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        val STORE = arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE)


        */
    }

    object PATH {
        val ARCHIVOS = Environment.getExternalStorageDirectory().path +
                File.separator + "rpv" +
                File.separator + APP.NAME +
                File.separator + "archivos"
        val LOGS = Environment.getExternalStorageDirectory().path +
                File.separator + "rpv" +
                File.separator + APP.NAME +
                File.separator + "log"
        /*String IMAGE = Environment.getExternalStorageDirectory().getPath()
            + File.separator + "rpv"
            + File.separator + APP.NAME
            + File.separator + "image"*/
        val PHOTO = Environment.getExternalStorageDirectory().path +
                File.separator + "rpv" +
                File.separator + APP.NAME +
                File.separator + "photo"
    }

    object RES {
        const val DENIED = "Denied"
        const val ERROR_DECRYPT = "Error Mobile Decrypt"
        const val ERROR_DESERIALIZE = "Error Mobile Deserialize"
        const val ERROR_ENCRYPT = "Error Mobile Encrypt"
        const val ERROR_INTERNET = "Error Internet"
        const val OBSOLETE = "Obsolete"
        const val OK = "Ok"
    }

    object INTENT {
        const val MODE = "mode"
        const val REPORT = "report"
        const val RECEIVER = "receiver"
    }

    object TIPO {
        const val TIPO_FUNCION = 2
    }

    object FOTO {
        const val GASTO = 1
        const val TICKET_VEHICULO = 2
        const val REVISIONES = 3
    }
}
