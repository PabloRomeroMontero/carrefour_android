package com.rpv.carrefour.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rpv.carrefour.data.local.dao.*
import com.rpv.carrefour.tipos.*

@Database(
    entities = [TipoUsuario::class, TipoApp::class, TipoTipo::class, TipoCentroHoy::class,  TipoVisita::class, TipoRespuestaPregunta::class,TipoReporteHoraNomina::class,TipoHoraNomina::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract val tipoUsuarioDao: TipoUsuarioDao
    abstract val tipoAppDao: TipoAppDao
    abstract val tipoTipoDao: TipoTipoDao
    abstract val tipoCentroDao: TipoCentroHoyDao
    abstract val tipoVisitaDao: TipoVisitaDao
    abstract val tipoRespuestaPreguntaDao: TipoRespuestaPreguntaDao
    abstract val tipoReporteHoraNominaDao: TipoReporteHoraNominaDao
    abstract val tipoHoraNominaDao: TipoHoraNominaDao


    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java,
                            "app_database"
                        )
                            .allowMainThreadQueries()
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }

    }

}