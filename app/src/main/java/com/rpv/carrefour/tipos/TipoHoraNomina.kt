package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject
import java.io.Serializable

@Entity
class TipoHoraNomina(
    @PrimaryKey var IdTipoHora : Int = 0,
    var Descripcion : String = "",
    var Productiva : Boolean = false,
    var Orden : Int = 0
) {

    override fun toString(): String {
        return "TipoReporteHoraNomina(IdTipoHora= $IdTipoHora)"

    }
}