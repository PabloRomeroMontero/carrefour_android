package com.rpv.carrefour.tipos

import androidx.annotation.Nullable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TipoCentroHoy (
    @PrimaryKey
    val idCentro : Int,
    val detalle : String,
    val idUsuario : Int,
    val semana : Int,
    val dia : Int,
    val nombre : String,
    val municipio : String,
    val domicilio : String,
    val provincia : String,
    val cCAA : String?,
    var hoy: Boolean
) {
    override fun toString(): String {
        return "TipoCentroHoy(idCentro=$idCentro, detalle='$detalle', idUsuario=$idUsuario, semana=$semana, dia=$dia, nombre='$nombre', municipio='$municipio', domicilio='$domicilio', provincia='$provincia', cCAA=$cCAA, hoy=$hoy)"
    }
}
