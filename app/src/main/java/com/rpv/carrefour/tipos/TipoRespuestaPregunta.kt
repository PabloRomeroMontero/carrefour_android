package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TipoRespuestaPregunta(
    @PrimaryKey  var idRespuesta: Int = 0,
    var idVisita: Int = 0,
    var idPregunta: Int = 0,
    var respuesta: String = ""
) {
    override fun toString(): String {
        return "TipoRespuestaPregunta(idRespuesta=$idRespuesta, idVisita=$idVisita, idPregunta=$idPregunta, respuesta='$respuesta')"
    }


}