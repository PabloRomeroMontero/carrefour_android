package com.rpv.carrefour.ui.elegirRuta

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.BuildConfig


import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.*
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import es.alesagal.recycleradapter.RecyclerAdapter
import com.rpv.carrefour.utils.*
import com.rpv.carrefour.utils.lottieAlertDialog.ClickListener
import com.rpv.carrefour.utils.lottieAlertDialog.LottieAlertDialog
import com.rpv.carrefour.utils.lottieAlertDialog.DialogTypes
import kotlinx.android.synthetic.main.fragment_elegir_ruta.*
import kotlinx.android.synthetic.main.item_rutas_recyclerview_login.view.*

class ElegirRutaFragment : Fragment(R.layout.fragment_elegir_ruta) {


    private var salir = true
    private val navController by lazy {
        findNavController()
    }
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }
    private lateinit var usuario: TipoUsuario
    private val args: ElegirRutaFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (salir) {
                    logoutPreguntar()
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }
            }
        })
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }


    //region [VIEWS]
    private fun setupViews() {
        setColorWindows(requireActivity())
        viewModel.getUsuariosList().observe(this.viewLifecycleOwner) {
            viewModel.refreshList()
            usuario = it[0]
            setupRcRutas(it)
        }
        swipeToRefreshRuta.setOnRefreshListener {
            refreshApiList()
            swipeToRefreshRuta.isRefreshing = false
        }
        btnLogOut.setOnClickListener { logoutPreguntar() }


    }

    private fun setupRcRutas(mList: List<TipoUsuario>) {
        val mAdapter = object : RecyclerAdapter<ViewHolderRut>(
            mList,
            R.layout.item_rutas_recyclerview_login,
            ViewHolderRut::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderRut, position: Int) {
                var item = mList[position]

                holder.lblItem.text = item.Detalle

                holder.clItem.setOnClickListener { navToOption(item) }
            }
        }
        rclRutas.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }

//endregion

    //region [UTILS]
    fun navToOption(tipoUsuario: TipoUsuario) {
        getCentros(tipoUsuario)
        var a = tipoUsuario.IdUsuario
        navController.navigate(
            ElegirRutaFragmentDirections.actionElegirRutaFragmentToElegirOpcionFragmentAction(
                tipoUsuario.IdPerfil,
                tipoUsuario.IdUsuario
            )
        )
    }

    private fun logoutPreguntar() {
        val alertDialog =
            LottieAlertDialog.Builder(requireContext(), DialogTypes.TYPE_WARNING).apply {
                setTitle("Salir")
                    .setDescription("¿Desea salir y cerrar sesión?")
                    .setPositiveText("Cerrar sesión")
                    .setNegativeText("Continuar")
                    .setPositiveButtonColor(Color.RED)
                    .setPositiveTextColor(Color.WHITE)
                    .setNegativeButtonColor(Color.parseColor("#008DCF"))
                    .setNegativeTextColor(Color.WHITE)
                    // Error View
                    .setPositiveListener(object : ClickListener {
                        override fun onClick(dialog: LottieAlertDialog) {
                            salir = false
                            viewModel.deleteUsers()
                            requireActivity().onBackPressed()
                            dialog.dismiss()
                        }
                    })
                    .setNegativeListener(object : ClickListener {
                        override fun onClick(dialog: LottieAlertDialog) {
                            dialog.dismiss()
                        }
                    })
            }
        alertDialog.build().show()
    }

    //endregion

    //region [LLAMADAS]


    private fun refreshApiList() {
        viewModel.refreshList()

        val s = Gson().toJson(TipoLogin().apply {
            login = args.user
            pass = args.password
            app = C.APP.NAME
        })

        val peticion = TipoPeticion().apply {
            peticion = WS.Get.LOGIN
            data = F.Security.encrypt(s, C.KEY.KEY)
        }


        WS(activity, peticion, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }

                lateinit var usuarios: ArrayList<TipoUsuario>
                val gson = Gson()
                try {
                    val itemTipoUsuario = object : TypeToken<ArrayList<TipoUsuario>>() {}.type
                    usuarios = gson.fromJson<ArrayList<TipoUsuario>>(dec, itemTipoUsuario)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DESERIALIZE)
                    return
                }

                if (usuarios[0].IdUsuario == 0) {
                    dialog("Error de autentificación.")
                    return
                }
                F.logout(requireContext())
                usuarios.forEach {
                    viewModel.insertarUsuario(it)
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

    fun getCentros(tipoUsuario: TipoUsuario) {
        viewModel.deleteTipoCentrosHoy()
        val petitionToday = TipoPeticion(
            WS.Get.CENTROS_BY_USERID,
            tipoUsuario.IdUsuario.toString(),
            BuildConfig.VERSION_CODE
        )
        val petitionNotToday = TipoPeticion(
            WS.Get.TODOS_CENTROS_BY_USERID,
            tipoUsuario.IdUsuario.toString(),
            BuildConfig.VERSION_CODE
        )

        WS(activity, petitionToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }

                if (dec != "") {
                    lateinit var centros: ArrayList<TipoCentro>
                    val gson = Gson()
                    try {
                        val itemTipoCentro = object : TypeToken<ArrayList<TipoCentro>>() {}.type
                        centros = gson.fromJson<ArrayList<TipoCentro>>(dec, itemTipoCentro)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                    centros.forEach {
                        viewModel.insertTipoCentroHoy(
                            TipoCentroHoy(
                                it.idCentro,
                                it.detalle,
                                it.idUsuario,
                                it.semana,
                                it.dia,
                                it.nombre,
                                it.municipio,
                                it.domicilio,
                                it.provincia,
                                it.cCAA,
                                true
                            )
                        )
                    }
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })

        WS(activity, petitionNotToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }

                if (dec != "") {
                    lateinit var centros: ArrayList<TipoCentro>
                    val gson = Gson()
                    try {
                        val itemTipoCentro = object : TypeToken<ArrayList<TipoCentro>>() {}.type
                        centros = gson.fromJson<ArrayList<TipoCentro>>(dec, itemTipoCentro)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                    centros.forEach {
                        viewModel.insertTipoCentroHoy(
                            TipoCentroHoy(
                                it.idCentro,
                                it.detalle,
                                it.idUsuario,
                                it.semana,
                                it.dia,
                                it.nombre,
                                it.municipio,
                                it.domicilio,
                                it.provincia,
                                it.cCAA,
                                false
                            )
                        )
                    }
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

    //endregion

}


class ViewHolderRut(view: View) : RecyclerView.ViewHolder(view) {
    var lblItem: TextView = view.lblItemRutasLoginFragment
    var clItem: View = view.clRutasItem
}