package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.rpv.carrefour.tipos.TipoCentro
import com.rpv.carrefour.tipos.TipoCentroHoy

@Dao
interface TipoCentroHoyDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTipoCentroHoy(tipoCentroHoy: TipoCentroHoy): Long

    @Query("SELECT * FROM TipoCentroHoy")
    fun getAllCentrosList(): LiveData<List<TipoCentroHoy>>


    @Query("SELECT * FROM TipoCentroHoy WHERE hoy=:hoy")
    fun getCentrHoyoNotToday(hoy: Boolean): LiveData<List<TipoCentroHoy>>


    @Query("DELETE FROM TipoCentroHoy")
    fun deleteTipoCentrosHoy()

    @Query("SELECT * FROM TipoCentroHoy WHERE idCentro = :idCentro")
    fun getCentroById(idCentro: String): TipoCentroHoy

    @Update
    fun updateCentroHoy(tipoCentroHoy: TipoCentroHoy): Int

}