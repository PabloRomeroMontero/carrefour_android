package com.rpv.carrefour.tipos

data class TipoCentro (
    val idCentro : Int,
    val detalle : String,
    val idUsuario : Int,
    val semana : Int,
    val dia : Int,
    val nombre : String,
    val municipio : String,
    val domicilio : String,
    val provincia : String,
    val cCAA : String?
) {
    override fun toString(): String {
        return "TipoCentro(detalle='$detalle', idUsuario=$idUsuario, idCentro=$idCentro, semana=$semana, dia=$dia, nombre='$nombre', municipio='$municipio', domicilio='$domicilio', provincia='$provincia', cCAA='$cCAA')"
    }
}


