package com.rpv.carrefour.utils.lottieAlertDialog

import com.rpv.carrefour.tipos.TipoCentroHoy

interface ClickListenerSearchCenterItem {
    fun onClick(dialog: LottieAlertDialogSearchCenter, item: TipoCentroHoy)
}