package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.TipoApp

@Dao
interface TipoAppDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAppVersion(appVersion: TipoApp): Long

    @Query("SELECT * FROM TipoApp")
    fun getAppVersion(): LiveData<List<TipoApp>>


    @Query("DELETE FROM TipoApp")
    fun deleteAppVersion()
}
