package com.rpv.carrefour.ui.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.utils.lottieAlertDialog.LottieAlertDialog
import com.rpv.carrefour.BuildConfig
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.*
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import com.rpv.carrefour.utils.*
import com.rpv.carrefour.utils.lottieAlertDialog.DialogTypes
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONArray


class LoginFragment : Fragment(R.layout.fragment_login) {
    private val navController by lazy {
        findNavController()
    }

    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        update()
    }

    private fun iniciarSesionFake() {
        val alertDialog =
            LottieAlertDialog.Builder(requireContext(), DialogTypes.TYPE_LOADING).apply {
                setTitle("Inicio Sesión")
                    .setDescription("Iniciando sesión automaticamente")
            }
        getTipos()
        val dialog = alertDialog.build()
        dialog.show()
        Handler().postDelayed({
            navController.navigate(
                LoginFragmentDirections.actionLoginFragmentToElegirRutaFragment(
                    viewModel.getLastUser().usuarioPassword,
                    viewModel.getLastUser().contrasenha
                )
            )
            dialog.dismiss()
        }, 2500)
    }

    //region [[LLAMADAS]]
    private fun update() {

        val peticion = TipoPeticion().apply {
            peticion = WS.Get.APPVERSION
            data = ""
        }

        WS(context, peticion, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {

                //region [Paso 1: Desencriptado]
                val dec: String

                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
                //endregion

                //region [Paso 2: Parseamos el JSON a objeto]
                val app : List<TipoApp>
                val itemTipoApp =
                    object : TypeToken<ArrayList<TipoApp>>() {}.type
                try {
                    app = Gson().fromJson(dec,itemTipoApp)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DESERIALIZE)
                    return
                }
                //endregion

                //region [Paso 3: Verificamos que el resultado es valido y tratamos los datos]
                var result = 0
                val ver = app.last().version
                val versionNumber = BuildConfig.VERSION_CODE
                if (ver > versionNumber) result = 1


                proceedAfterCheck(result)
                //endregion
            }
            override fun onError(respuesta: TipoRespuesta) {
                //Toast.makeText(this, respuesta.estado, Toast.LENGTH_SHORT).show()
                //dialog(respuesta.estado)
            }
            override fun onDenied(respuesta: TipoRespuesta) {
                //Toast.makeText(this, respuesta.estado, Toast.LENGTH_SHORT).show()
                //dialog(respuesta.respuesta)
            }
            override fun onObsolete(respuesta: TipoRespuesta) {
                //F.onObsolete(this@InicioActivity)
            }
        }, false)
    }

    fun login() {
        if (validated()) {
            viewModel.deleteUsers()
            val s = TipoLogin().apply {
                login = editTextUsuario.text.toString()
                pass = editTextContraseña.text.toString()
                app = C.APP.NAME
            }.toJson()

            val peticion = TipoPeticion().apply {
                peticion = WS.Get.LOGIN
                data = F.Security.encrypt(s, C.KEY.KEY)
            }

            WS(activity, peticion, object : WS.Callback {

                override fun onCompleted(respuesta: TipoRespuesta) {
                    getTipos()
                    val dec: String
                    try {
                        dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DECRYPT)
                        return
                    }
                    if (dec != "") {
                        lateinit var usuarios: ArrayList<TipoUsuario>
                        val gson = Gson()
                        try {
                            val itemTipoUsuario =
                                object : TypeToken<ArrayList<TipoUsuario>>() {}.type
                            usuarios = gson.fromJson<ArrayList<TipoUsuario>>(dec, itemTipoUsuario)
                        } catch (e: Exception) {
                            log(e)
                            dialog(C.RES.ERROR_DESERIALIZE)
                            return
                        }

                        viewModel.deleteUsers()
                        usuarios.forEach {
                            it.usuarioPassword = editTextUsuario.text.toString()
                            it.contrasenha = editTextContraseña.text.toString()
                            viewModel.insertarUsuario(it)
                        }

                        navController.navigate(
                            LoginFragmentDirections.actionLoginFragmentToElegirRutaFragment(
                                editTextUsuario.text.toString(),
                                editTextContraseña.text.toString()
                            )
                        )
                    } else {
                        dialog("Error de autentificación.")
                        return
                    }
                }

                override fun onError(respuesta: TipoRespuesta) {
                    dialog(respuesta.estado)
                }

                override fun onDenied(respuesta: TipoRespuesta) {
                    dialog(respuesta.respuesta)
                }

                override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                    dialog(respuesta.respuesta)
                }

            })

        }
    }

    fun getTipos() {
        val petition = TipoPeticion(WS.Get.TIPOS, "", BuildConfig.VERSION_CODE)
        WS(activity, petition, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
                Log.d("dec", dec)


                if (dec != "") {
                    viewModel.deleteTipos()
                    lateinit var tipos: ArrayList<TipoTipo>
                    val gson = Gson()
                    try {
                        val itemTipoTipo = object : TypeToken<ArrayList<TipoTipo>>() {}.type
                        tipos = gson.fromJson<ArrayList<TipoTipo>>(dec, itemTipoTipo)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                    tipos.forEach {
                        var prueba = it
                        viewModel.insterTipo(it)
                    }
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

    //endregion [[LLAMADAS]]

    @SuppressLint("SetTextI18n")
    private fun setupViews() {
        setColorWindows(requireActivity())
        if (!viewModel.userIsEmpty()) {
            iniciarSesionFake()
        }
        textViewVersion.text = "VERSION " + BuildConfig.VERSION_NAME
        btnLogin.setOnClickListener { login() }
    }

    private fun validated(): Boolean {
        return if (editTextUsuario.text.isBlank()) {
            dialog("El campo usuario está vacio")
            false
        } else if (editTextContraseña.text.isBlank()) {
            dialog("El campo contraseña está vacio")
            false
        } else {
            true
        }
    }

    private fun proceedAfterCheck(result: Int) {
        when (result) {
            0 ->  {
                setupViews()
            }
            1 -> startActivity(Intent(context,UpdateActivity::class.java))
        }
    }

}