package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.TipoApp
import com.rpv.carrefour.tipos.TipoReporteHoraNomina

@Dao
interface TipoReporteHoraNominaDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertReporteHoraNomina(reporteHoraNomina: TipoReporteHoraNomina): Long

    @Query("SELECT * FROM TipoReporteHoraNomina")
    fun getReporteHoraNominaList() :LiveData<List<TipoReporteHoraNomina>>


    @Query("DELETE FROM TipoReporteHoraNomina")
    fun deleteReporteHoraNomina()
}