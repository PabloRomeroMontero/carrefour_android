package com.rpv.carrefour.ui.horasNominas

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.BuildConfig
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.*
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import com.rpv.carrefour.utils.*
import es.alesagal.recycleradapter.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_horas_nominas.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

import kotlin.collections.ArrayList

class HorasNominasFragment : Fragment(R.layout.fragment_horas_nominas) {
    private val navController by lazy {
        findNavController()
    }
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }
    private var listReporteHoraNomina: List<TipoReporteHoraNomina> = listOf()
    private var listTiposHoraNomina: List<TipoHoraNomina> = listOf()
    private var listSecciones: List<String> = listOf()
    var listaAuxPintar = mutableMapOf<String,String>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        setColorWindows(requireActivity())
        toolbarHorasNomina.title = "Horas Nomina"
        toolbarHorasNomina.setTitleTextColor(Color.WHITE)
        floatingActionButtonHorasNomina.setOnClickListener {
            navController.navigate(HorasNominasFragmentDirections.actionHorasNominasFragmentDestinationToAddHorasNominaFragmentDirections())
        }
        viewModel.getHoraNominaList().observe(viewLifecycleOwner) {
            this.listTiposHoraNomina = it
        }
        viewModel.getReporteHoraNominaList().observe(viewLifecycleOwner) {
            this.listReporteHoraNomina = it
            setupRcPersonaNominas(listReporteHoraNomina.sortedByDescending { item ->
                LocalDate.parse(
                    item.FechaHoras,
                    DateTimeFormatter.ofPattern("dd/MM/yyyy")
                )
            })        }
        viewModel.getListaSeccion().observe(viewLifecycleOwner) {
            this.listSecciones = it
        }
        requestSeccionesTiendaNomina()
        requestTipoHoraNomina()
    }

    private fun requestTipoHoraNomina() {
        val petitionToday = TipoPeticion(
            WS.Get.GetTipoHoraNomina,
            "",
            BuildConfig.VERSION_CODE
        )


        WS(activity, petitionToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                viewModel.deleteHoraNomina()
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }

                lateinit var horasNomina: ArrayList<TipoHoraNomina>
                try {
                    val itemTipoHoraNomina = object : TypeToken<ArrayList<TipoHoraNomina>>() {}.type
                    horasNomina =
                        Gson().fromJson<ArrayList<TipoHoraNomina>>(dec, itemTipoHoraNomina)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DESERIALIZE)
                    return
                }

                for (i in horasNomina) {
                    viewModel.insertHoraNomina(i)
                }
                Log.d("dec-horaNomina", dec)
                requestReportesHorasNomina()
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

    private fun requestSeccionesTiendaNomina() {
        val petitionToday = TipoPeticion(
            WS.Get.GetSeccionesNomina,
            Gson().toJson(viewModel.getLastUser()),
            BuildConfig.VERSION_CODE
        )


        WS(activity, petitionToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                viewModel.clearListaSecciones()
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }

                lateinit var seccionNomina: ArrayList<String>
                try {
                    val itemString = object : TypeToken<ArrayList<String>>() {}.type
                    seccionNomina =
                        Gson().fromJson<ArrayList<String>>(dec, itemString)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DESERIALIZE)
                    return
                }

                viewModel.addSecciones(seccionNomina)
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        })
    }

    private fun requestReportesHorasNomina() {

        val petitionToday = TipoPeticion(
            WS.Get.GetHorasNomina,
            Gson().toJson(viewModel.getLastUser()),
            BuildConfig.VERSION_CODE
        )

        WS(activity, petitionToday, object : WS.Callback {

            override fun onCompleted(respuesta: TipoRespuesta) {
                viewModel.deleteReporteHoraNomina()
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
                lateinit var reporteHorasNomina: ArrayList<TipoReporteHoraNomina>
                try {
                    val itemReporteTipoHoraNomina =
                        object : TypeToken<ArrayList<TipoReporteHoraNomina>>() {}.type
                    reporteHorasNomina =
                        Gson().fromJson<ArrayList<TipoReporteHoraNomina>>(
                            dec,
                            itemReporteTipoHoraNomina
                        )
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DESERIALIZE)
                    return
                }
                for (i in reporteHorasNomina) {
                    viewModel.insertReporteHoraNomina(i)
                }
                Log.d("dec-horaReporteNomina", dec)
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }
        })
    }

    fun setupRcPersonaNominas(mList: List<TipoReporteHoraNomina>) {
        val mAdapter = object : RecyclerAdapter<ViewHolderPersonaNomina>(
            mList.slice(1..20),
            R.layout.item_persona_horas_nomina,
            ViewHolderPersonaNomina::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderPersonaNomina, position: Int) {
                holder.setIsRecyclable(false)
                val item = mList[position]
                holder.lblFecha.apply {
                    text = item.FechaHoras
                    if (!listaAuxPintar.containsValue(item.FechaHoras)) {
                        visibility = View.VISIBLE
                        listaAuxPintar[item.IdPersona] = item.FechaHoras
                    } else if(listaAuxPintar.containsKey(item.IdPersona) && listaAuxPintar.containsValue(item.FechaHoras)){
                        visibility = View.VISIBLE
                    }else{
                        visibility = View.GONE
                    }
                }
//                holder.clParent.apply {
//                    visibility =
//                        if (listaAuxPintar.containsValue(item.IdPersona) && listaAuxPintar[item.IdPersona] == item.FechaHoras) {
//                            View.GONE
//                        } else {
//                            View.VISIBLE
//                        }
//                }
                holder.lblFecha.text = mList[position].FechaHoras
                holder.lblNombre.text = mList[position].Nombre

//                if (listaAuxPintar.containsValue(item.IdPersona) && listaAuxPintar[item.IdPersona] == item.FechaHoras) {
//                    setupRcHorasNomina(
//                        listReporteHoraNomina.filter {
//                            it.IdPersona == mList[position].IdPersona &&
//                                    it.FechaHoras == mList[position].FechaHoras
//                        },
//                        holder.rcHoras,
//                        holder.clParent
//                    )
//                }
            }
        }

        rcHorasNominasFragment.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }



    fun setupRcHorasNomina(
        listRcPersonasNomina: List<TipoReporteHoraNomina>,
        rcHoras: RecyclerView,
        clParent: ConstraintLayout
    ) {
        val mAdapter = object : RecyclerAdapter<ViewHolderHorasPersonaNomina>(
            listRcPersonasNomina,
            R.layout.item_hora_nomina,
            ViewHolderHorasPersonaNomina::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderHorasPersonaNomina, position: Int) {
                holder.lblNombre.text = listRcPersonasNomina[position].Nombre
            }
        }
        rcHoras.apply {
            setHasFixedSize(true)
            //addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }
}

class ViewHolderPersonaNomina(val view: View) : RecyclerView.ViewHolder(view) {
    val clParent = view.findViewById<ConstraintLayout>(R.id.clParentPersonaHorasNomina)!!
    val lblFecha = view.findViewById<TextView>(R.id.lblFechaPersonaHoraNomina)!!
    val lblNombre = view.findViewById<TextView>(R.id.lblPersonaHoraNomina)!!
    val rcHoras = view.findViewById<RecyclerView>(R.id.rcHorasNomina)!!
}

class ViewHolderHorasPersonaNomina(val view: View) : RecyclerView.ViewHolder(view) {
    val clParent = view.findViewById<ConstraintLayout>(R.id.clParent_HoraNomina)!!
    val lblNombre = view.findViewById<TextView>(R.id.lblAnhadirHorasNomina)!!
    val btnDelete = view.findViewById<ImageView>(R.id.btnDeleteHoraNomina)!!
}


fun TipoReporteHoraNomina.isReporteUnico(item : TipoReporteHoraNomina): Boolean {
    return  this.IdPersona == item.IdPersona &&
            this.IdHorasNomina == item.IdHorasNomina
}