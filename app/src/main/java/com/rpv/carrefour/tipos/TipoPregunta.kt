package com.rpv.carrefour.tipos


data class TipoPregunta(
    val IdPregunta: Int,
    val Pregunta: String,
    val IdTipoPregunta: Int,
    val Criterio: String,
    val Orden: Int,
    val Seccion: String
) {


    override fun toString(): String {
        return "TipoPRegunta(idPregunta=$IdPregunta, pregunta='$Pregunta', idTipoPregunta=$IdTipoPregunta, criterio='$Criterio', orden=$Orden, seccion=$Seccion)"
    }

}