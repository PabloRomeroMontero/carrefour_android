package com.rpv.carrefour.utils

import android.content.Context
import android.content.SharedPreferences

object P {

    private var instance: SharedPreferences? = null
    private const val pName = "prefs"

    fun init(context: Context) {
        if (instance == null) {
            instance = context.getSharedPreferences(pName, Context.MODE_PRIVATE)
        }
    }

    @Synchronized
    private fun getInstance(): SharedPreferences {
        return instance ?: throw RuntimeException("Initialize P")
    }

    enum class I {
        // USER, PERFIL, HV,
        FOTO
    }

    enum class S {
        // NOMBRE, DETALLE, FUNCIONES, TOPICS, PERSONA, MATRICULA
    }

    enum class L {
        LOCATION_TIME_LAPSE
    }

    operator fun get(settingName: I): Int {
        return getInstance().getInt(settingName.toString(), 0)
    }

    operator fun get(settingName: S): String {
        return getInstance().getString(settingName.toString(), "") ?: ""
    }

    operator fun get(settingName: L): Long {
        return getInstance().getLong(settingName.toString(), 0L)
    }

    operator fun set(settingName: I, value: Int) {
        getInstance().edit().putInt(settingName.toString(), value).apply()
    }

    operator fun set(settingName: S, value: String) {
        getInstance().edit().putString(settingName.toString(), value).apply()
    }

    operator fun set(settingName: L, value: Long) {
        getInstance().edit().putLong(settingName.toString(), value).apply()
    }

    fun inc(settingName: I) {
        set(settingName, get(settingName) + 1)
    }

    fun clear() {
        getInstance().edit().clear().apply()
    }

}
