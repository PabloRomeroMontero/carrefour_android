package com.rpv.carrefour.ui.preguntas

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.RadioGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rpv.carrefour.utils.lottieAlertDialog.ClickListener
import com.rpv.carrefour.utils.lottieAlertDialog.DialogTypes
import com.rpv.carrefour.utils.lottieAlertDialog.LottieAlertDialog
import com.rpv.carrefour.BuildConfig
import com.rpv.carrefour.R
import com.rpv.carrefour.tipos.*
import com.rpv.carrefour.ui.viewModel.MainViewModel
import com.rpv.carrefour.ui.viewModel.MainViewModelFactory
import com.rpv.carrefour.utils.*
import es.alesagal.recycleradapter.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_preguntas.*
import kotlinx.android.synthetic.main.item_preguntas.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PreguntasFragment : Fragment(R.layout.fragment_preguntas) {
    private val args: PreguntasFragmentArgs by navArgs()
    private val navController by lazy {
        findNavController()
    }
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(requireContext())
    }
    private var listaPreguntas: List<TipoPregunta> = listOf()
    private var respuestas: ArrayList<TipoRespuestaPregunta> = ArrayList()
    private lateinit var visita: TipoVisita
    private var listaSecciones = mutableMapOf<Int,String>()
    private var salir = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                if (salir) {
                    getVisita(true)
                } else {
                    isEnabled = false
                    activity?.onBackPressed()
                }

            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        crearVisita(viewModel.getCentroById(args.idCentro))
    }

    private fun setupViews() {
        recordarRespuestas()
        viewModel.getListaPreguntas().observe(this.viewLifecycleOwner) { lista ->
            listaPreguntas = lista
            if (respuestas.size == 0 || respuestas.size != listaPreguntas.size) {
                respuestas = ArrayList()
                for (i in listaPreguntas) {
                    respuestas.add(TipoRespuestaPregunta())
                }
            } else {
                respuestas.sortBy { it.idPregunta }
            }
            setupRcPreguntas(listaPreguntas.sortedBy { it.Orden })
        }
        btn_save_preguntas.setOnClickListener {
            if (validarPreguntas()) {
                enviarRespuestas()
//                super.requireActivity().onBackPressed()
            } else {
                LottieAlertDialog.Builder(
                    context,
                    DialogTypes.TYPE_ERROR
                )
                    .setTitle("Respuestas Incompletas")
                    .setDescription("Existen preguntas sin contestar.")
                    .setPositiveText("Continuar")
                    .setPositiveListener(object : ClickListener {
                        override fun onClick(dialog: LottieAlertDialog) {
                            dialog.dismiss()
                        }
                    }).build().show()
            }
        }
    }

    //region    [RC]
    private fun setupRcPreguntas(mList: List<TipoPregunta>) {
        val mAdapter = object : RecyclerAdapter<ViewHolderPreguntas>(
            mList,
            R.layout.item_preguntas,
            ViewHolderPreguntas::class.java
        ) {
            override fun onBindViewHolder(holder: ViewHolderPreguntas, position: Int) {
                holder.setIsRecyclable(false)
                val item = mList[position]
                holder.lblPregunta.apply {
                    text = spannable { bold(item.Orden.toString() + " - " + item.Pregunta) }
                }
                holder.lblSeccion.apply {
                    text = item.Seccion
                    if (listaSecciones.containsKey(item.IdPregunta) && listaSecciones.containsValue(item.Seccion)) {
                        visibility = View.VISIBLE
                    } else if( !listaSecciones.containsValue(item.Seccion)){
                        visibility = View.VISIBLE
                        listaSecciones[item.IdPregunta] = item.Seccion
                    }else{
                        visibility = View.GONE
                    }
                }

                when (item.IdTipoPregunta) {
                    0 -> {
                        for (i in respuestas) {
                            if (item.IdPregunta == i.idPregunta)
                                holder.ediTtextPregunta.setText(i.respuesta)
                        }
                        holder.ediTtextPregunta.visibility = View.VISIBLE
                        holder.radioGroup.visibility = View.GONE
                        holder.spinnerPregunta.visibility = View.GONE
                        holder.lblContador.visibility = View.VISIBLE


                        holder.ediTtextPregunta.addTextChangedListener(object : TextWatcher {
                            override fun beforeTextChanged(
                                s: CharSequence?,
                                start: Int,
                                count: Int,
                                after: Int
                            ) {
                            }

                            override fun onTextChanged(
                                s: CharSequence?,
                                start: Int,
                                before: Int,
                                count: Int
                            ) {
                                respuestas[position] =
                                    TipoRespuestaPregunta(
                                        0,
                                        visita.idVisita,
                                        item.IdPregunta,
                                        s.toString()
                                    )
                                holder.lblContador.text = s!!.count().toString() + "/250"

                            }

                            override fun afterTextChanged(s: Editable?) {
                            }
                        })
                    }
                    1 -> {

                        holder.ediTtextPregunta.visibility = View.GONE
                        holder.radioGroup.visibility = View.VISIBLE
                        holder.spinnerPregunta.visibility = View.GONE
                        holder.lblContador.visibility = View.GONE

                        val criterio: List<String> = if (item.Criterio.isNotEmpty()) {
                            item.Criterio.split(";")
                        } else {
                            "Si;No".split(";")
                        }
                        for (button in criterio) {
                            val rb = RadioButton(context).apply {

                                for (i in respuestas) {
                                    if (item.IdPregunta == i.idPregunta)
                                        if (i.respuesta == button)
                                            isChecked = true
                                }
                                text = button
                                id = View.generateViewId()
                                setPadding(0, 0, 12, 0)
                                setOnCheckedChangeListener { buttonView, isChecked ->
                                    respuestas[position] = TipoRespuestaPregunta(
                                        0,
                                        visita.idVisita,
                                        item.IdPregunta,
                                        buttonView.text.toString()
                                    )
                                }
                            }

                            holder.radioGroup.addView(rb)
                        }
                    }
                    2 -> {
                        var opciones = item.Criterio.split(";")
                        holder.ediTtextPregunta.visibility = View.GONE
                        holder.radioGroup.visibility = View.GONE
                        holder.spinnerPregunta.visibility = View.VISIBLE
                        holder.lblContador.visibility = View.GONE


                        loadSpinnerString(opciones, holder.spinnerPregunta)
                        holder.spinnerPregunta.apply {
                            for (i in respuestas) {
                                if (item.IdPregunta == i.idPregunta)
                                    for ((index, i) in opciones.withIndex()) {
                                        if (i == respuestas[position].respuesta) {
                                            setSelection(index)
                                        }
                                    }
                            }
                            this.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onNothingSelected(parent: AdapterView<*>?) {
                                    }

                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        positionSpinner: Int,
                                        id: Long
                                    ) {
                                        respuestas[position] = TipoRespuestaPregunta(
                                            0,
                                            visita.idVisita,
                                            item.IdPregunta,
                                            holder.spinnerPregunta.selectedItem.toString()
                                        )
                                        Log.d("logjeje", respuestas[position].toString())
                                    }
                                }
                        }
                    }
                }
            }
        }
        rcPreguntas.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }
    //endregion

    //region [LLAMADAS]
    private fun getPreguntas() {

        val petition = TipoPeticion(
            WS.Get.PREGUNTAS,
            "",
            BuildConfig.VERSION_CODE
        )
        WS(activity, petition, object : WS.Callback {
            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    lateinit var preguntas: ArrayList<TipoPregunta>
                    val gson = Gson()
                    try {
                        val itemTipoPregunta =
                            object : TypeToken<ArrayList<TipoPregunta>>() {}.type
                        preguntas =
                            gson.fromJson<ArrayList<TipoPregunta>>(dec, itemTipoPregunta)
                        viewModel.addPreguntas(preguntas)
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
            }


            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        }, true)
    }

    private fun enviarRespuestas() {

        for (respuesta in respuestas)
            respuesta.idVisita = visita.idVisita

        var respuestasGson = Gson().toJson(respuestas)
        val petition = TipoPeticion(
            WS.Set.SEND_RESPUESTA,
            respuestasGson,
            BuildConfig.VERSION_CODE
        )
        WS(activity, petition, object : WS.Callback {
            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    try {
                        LottieAlertDialog.Builder(
                            context,
                            DialogTypes.TYPE_SUCCESS
                        )
                            .setTitle("Completado")
                            .setDescription("Se han añadido las respuestas correctamente.")
                            .setPositiveText("Continuar")
                            .setPositiveListener(object : ClickListener {
                                override fun onClick(dialog: LottieAlertDialog) {
                                    dialog.dismiss()
                                }
                            }).build().show()
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
            }


            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        }, true)
    }
    //endregion

    //region [UTILS]


    private fun volver() {
        salir = false

        viewModel.clearListaPreguntas()
        respuestas.clear()
        requireActivity().onBackPressed()
    }

    private fun loadSpinnerString(list: List<String>, spinner: Spinner) {
        val aa = ArrayAdapter(requireContext(), R.layout.simple_spinner_item, list)
        aa.setDropDownViewResource(R.layout.lblspinner_preguntas)
        spinner.adapter = aa
    }

    private fun setupToolbar() {
        toolbarPreguntas.title = viewModel.getCentroById(args.idCentro).nombre
        toolbarPreguntas.setTitleTextColor(Color.WHITE)
    }

    private fun validarPreguntas(): Boolean {
        var estanContestadas = false
        if (respuestas.isNotEmpty()) {
            for (respuesta in respuestas) {
                if (respuesta.respuesta != "") {
                    estanContestadas = true
                } else {
                    estanContestadas = false
                    break
                }
            }
        }
        return estanContestadas
    }
    //endregion

    //region [VISITA]
    @SuppressLint("SimpleDateFormat")
    fun crearVisita(item: TipoCentroHoy) {

        val cal = Calendar.getInstance()
        val timeSetListener =
            TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                timePicker.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.azulClaro
                    )
                )

                val visitaDialog = TipoVisita(
                    0,
                    viewModel.getUserById(args.idUser).IdPersona.toInt(),
                    args.idUser,
                    item.idCentro,
                    SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().time),
                    SimpleDateFormat("HH:mm").format(cal.time),
                    obtenerFechaConFormato("HH:mm", Locale.ROOT.country),
                    "",
                    "",
                    false
                )
                visita = visitaDialog
                enviarNuevaVisita()
                setupViews()
            }

        TimePickerDialog(
            context, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(
                Calendar.MINUTE
            ), true
        ).apply {
            setOnCancelListener {
                volver()
            }
            show()
        }


    }

    //HECHO
    private fun enviarNuevaVisita() {
        getVisita(false)
    }

    @SuppressLint("SimpleDateFormat")
    fun salirVisita(item: TipoCentroHoy, visita: TipoVisita) {
        if (visita.horaSalida != "") {
            val alertDialog =
                LottieAlertDialog.Builder(requireContext(), DialogTypes.TYPE_WARNING).apply {
                    setTitle("Cerrar visita en " + item.nombre)
                        .setDescription("Cerró la visita por última vez a las " + visita.horaSalida + "h\n¿Desea cambiarla?")
                        .setPositiveText("Cambiar")
                        .setNegativeText("Salir")
                        .setPositiveButtonColor(Color.RED)
                        .setPositiveTextColor(Color.WHITE)
                        .setNegativeButtonColor(Color.parseColor("#008DCF"))
                        .setNegativeTextColor(Color.WHITE)
                        // Error View
                        .setPositiveListener(object : ClickListener {
                            override fun onClick(dialog: LottieAlertDialog) {
                                val cal = Calendar.getInstance()
                                val timeSetListener =
                                    TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                                        cal.set(Calendar.HOUR_OF_DAY, hour)
                                        cal.set(Calendar.MINUTE, minute)
                                        timePicker.setBackgroundColor(
                                            ContextCompat.getColor(
                                                requireContext(),
                                                R.color.azulClaro
                                            )
                                        )
                                        visita.horaSalida =
                                            SimpleDateFormat("HH:mm").format(cal.time)
                                        visita.horaRealSalida =
                                            obtenerFechaConFormato("HH:mm", Locale.ROOT.country)
                                        enviarVisitaUpdateada()
                                        volver()
                                    }

                                TimePickerDialog(
                                    context,
                                    timeSetListener,
                                    cal.get(Calendar.HOUR_OF_DAY),
                                    cal.get(
                                        Calendar.MINUTE
                                    ),
                                    true
                                ).apply {
                                    setTitle(viewModel.getCentroById(args.idCentro).nombre)
                                    setOnCancelListener {
                                        it.dismiss()
                                    }
                                    show()
                                }
                                dialog.dismiss()
                            }
                        })
                        .setNegativeListener(object : ClickListener {
                            override fun onClick(dialog: LottieAlertDialog) {
                                volver()
                                dialog.dismiss()
                            }
                        })
                }
            alertDialog.build().show()
        } else {
            val cal = Calendar.getInstance()
            val timeSetListener =
                TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                    cal.set(Calendar.HOUR_OF_DAY, hour)
                    cal.set(Calendar.MINUTE, minute)
                    timePicker.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.azulClaro
                        )
                    )
                    visita.horaSalida =
                        SimpleDateFormat("HH:mm").format(cal.time)
                    visita.horaRealSalida =
                        obtenerFechaConFormato("HH:mm", Locale.ROOT.country)
                    enviarVisitaUpdateada()
                    volver()
                }

            TimePickerDialog(
                context,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(
                    Calendar.MINUTE
                ),
                true
            ).apply {
                setTitle(viewModel.getCentroById(args.idCentro).nombre)
                setOnCancelListener {
                    it.dismiss()
                }
                show()
            }
        }


    }

    private fun enviarVisitaUpdateada() {
        var visitaSer = Gson().toJson(visita)
        val petition = TipoPeticion(
            WS.Set.SEND_CLOSE_VISITA,
            visitaSer,
            BuildConfig.VERSION_CODE
        )
        WS(activity, petition, object : WS.Callback {
            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    if (dec.toInt() != 0)
                        viewModel.updateTipoVisita(visita)

                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
            }


            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        }, true)
    }

    private fun getVisita(b: Boolean) {

        // METODO QUE RECIBE Y ENVIA VISITAS UPDATEADAS
        val visitaSer = Gson().toJson(visita)
        val petition = TipoPeticion(
            WS.Set.SEND_VISITA,
            visitaSer,
            BuildConfig.VERSION_CODE
        )

        WS(activity, petition, object : WS.Callback {
            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    Log.d("idVisita", dec)
                    val itemTipoVisita =
                        object : TypeToken<List<TipoVisita>>() {}.type
                    var visitaArr = Gson().fromJson<List<TipoVisita>>(dec, itemTipoVisita)
                    for (i in visitaArr) {
                        visita = i
                        viewModel.insertTipoVisita(visita)
                    }
                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }


                if (b) {
                    salirVisita(viewModel.getCentroById(args.idCentro), visita)
                }
            }

            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        }, true)
    }

    @SuppressLint("SimpleDateFormat")
    fun obtenerFechaConFormato(formato: String, zonaHoraria: String): String {
        val calendar = Calendar.getInstance()
        val date = calendar.time
        val sdf = SimpleDateFormat(formato)
        return sdf.format(date)
    }

//endregion

    //region    [RECUERDO PREGUNTAS]
    fun recordarRespuestas() {

        val petition = TipoPeticion(
            WS.Get.RECUERDO_RESPUESTAS,
            visita.idCentro.toString(),
            BuildConfig.VERSION_CODE
        )
        WS(activity, petition, object : WS.Callback {
            override fun onCompleted(respuesta: TipoRespuesta) {
                val dec: String
                try {
                    dec = F.Security.decrypt(respuesta.respuesta, C.KEY.KEY)
                    val gson = Gson()
                    try {
                        val itemTipoRespuestaPregunta =
                            object : TypeToken<ArrayList<TipoRespuestaPregunta>>() {}.type
                        var respuestasPreguntas =
                            gson.fromJson<ArrayList<TipoRespuestaPregunta>>(
                                dec,
                                itemTipoRespuestaPregunta
                            )
                        if (respuestasPreguntas[0].respuesta != "") {
                            respuestas = respuestasPreguntas
                        }

                        getPreguntas()
                    } catch (e: Exception) {
                        log(e)
                        dialog(C.RES.ERROR_DESERIALIZE)
                        return
                    }

                } catch (e: Exception) {
                    log(e)
                    dialog(C.RES.ERROR_DECRYPT)
                    return
                }
            }


            override fun onError(respuesta: TipoRespuesta) {
                dialog(respuesta.estado)
            }

            override fun onDenied(respuesta: TipoRespuesta) {
                dialog(respuesta.respuesta)
            }

            override fun onObsolete(respuesta: TipoRespuesta) {
//                    F.onObsolete(activity)
                dialog(respuesta.respuesta)
            }

        }, true)

    }
    //endregion

}


class ViewHolderPreguntas(view: View) : RecyclerView.ViewHolder(view) {

    var lblSeccion: TextView = view.textViewSeccionItemPregunta
    var lblPregunta: TextView = view.lblTipoPreguntaTextoLibre

    //Tipo 0
    var ediTtextPregunta: EditText = view.editTextTipoPreguntaTextoLibre
    var lblContador: TextView = view.lblContador_observaciones_item_preguntas

    //Tipo 1
    var radioGroup: RadioGroup = view.radioGroupPreguntaSiNo

    //Tipo 2
    var spinnerPregunta: Spinner = view.spnTipoPreguntaValores
}