package com.rpv.carrefour.tipos


import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject

@Entity
data class TipoUsuario(
    @PrimaryKey var IdUsuario: Int,
    var IdPersona: String,
    var IdPerfil: Int,
    var Nombre: String,
    var Detalle: String,
    var Funciones: String,
    var Topics: String,
    var IdPersonaResponsable: String,
    var Titular: Boolean,
    var usuarioPassword: String = "",
    var contrasenha: String = ""
) {
    fun toJson() = JSONObject().apply {
        put("idUsuario", IdUsuario)
        put("idPersona", IdPersona)
        put("idPerfil", IdPerfil)
        put("nombre", Nombre)
        put("detalle", Detalle)
        put("funciones", Funciones)
        put("topics", Topics)
        put("idPersonaResponsable", IdPersonaResponsable)
        put("titular", Titular)
    }.toString()

}
