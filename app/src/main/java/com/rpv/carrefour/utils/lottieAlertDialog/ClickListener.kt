package com.rpv.carrefour.utils.lottieAlertDialog

interface ClickListener {
    fun onClick(dialog : LottieAlertDialog)
}