package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject
import java.io.Serializable

@Entity
class TipoReporteHoraNomina(
    var IdPersona : String = "",
    var Nombre : String = "",
    var IdUsuario : Int = 0,
    var Detalle : String = "",
    var FechaReporte : String = "",
    var FechaHoras : String = "",
    var TipoHoras : Int = 0,
    var DescTipoHoras : String = "",
    var Horas : String = "",
    var Seccion : String = "",
    var IdPersonaResponsable : String = "",
    @PrimaryKey
    var IdHorasNomina : Int = 0,
    var IdResponsable : Int = 0,
    var HorasDia : Double = 0.0,
    var HorasSemana : Double = 0.0

) : Serializable {

    constructor(o: JSONObject) : this(
            IdPersona  = o.optString("IdPersona"),
            Nombre  = o.optString("Nombre"),
            IdUsuario  = o.optInt("IdUsuario"),
            Detalle = o.optString("Detalle"),
            FechaReporte = o.optString("FechaReporte"),
            FechaHoras = o.optString("FechaHoras"),
            TipoHoras = o.optInt("TipoHoras"),
            DescTipoHoras = o.optString("DescTipoHoras"),
            Horas = o.optString("Horas"),
            Seccion = o.optString("Seccion"),
            IdPersonaResponsable = o.optString("IdPersonaResponsable"),
            IdHorasNomina  = o.optInt("IdHorasNomina"),
            IdResponsable  = o.optInt("IdResponsable"),
            HorasDia  = o.optDouble("HorasDia"),
            HorasSemana  = o.optDouble("HorasSemana")
    )

    fun toJson() = JSONObject().apply {
        put("IdPersona", IdPersona)
        put("Nombre", Nombre)
        put("IdUsuario", IdUsuario)
        put("Detalle", Detalle)
        put("FechaReporte", FechaReporte)
        put("FechaHoras", FechaHoras)
        put("TipoHoras", TipoHoras)
        put("DescTipoHoras", DescTipoHoras)
        put("Horas", Horas)
        put("Seccion", Seccion)
        put("IdPersonaResponsable", IdPersonaResponsable)
        put("IdHorasNomina", IdHorasNomina)
        put("IdResponsable", IdResponsable)
        put("HorasDia", HorasDia)
        put("HorasSemana", HorasSemana)

    }.toString()

    override fun toString(): String {
        return "TipoReporteHoraNomina(=$IdHorasNomina, $IdHorasNomina)"

    }

}
