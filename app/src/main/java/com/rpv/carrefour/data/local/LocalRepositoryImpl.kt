package com.rpv.carrefour.data.local

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.rpv.carrefour.data.local.dao.*
import com.rpv.carrefour.tipos.*

class LocalRepositoryImpl(
    private val tipoUsuarioDao: TipoUsuarioDao,
    private val tipoAppDao: TipoAppDao,
    private val tipoTipoDao: TipoTipoDao,
    private val tipoCentroHoyDao: TipoCentroHoyDao,
    private val tipoVisitaDao: TipoVisitaDao,
    private val tipoRespuestaPreguntaDao: TipoRespuestaPreguntaDao,
    private val tipoReporteHoraNominaDao: TipoReporteHoraNominaDao,
    private val tipoHoraNominaDao: TipoHoraNominaDao
) : Repository {
    override fun insertAppVersion(appVersion: TipoApp): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoAppDao.insertAppVersion(appVersion) }
        return 0
    }

    override fun getAppVersion(): LiveData<List<TipoApp>> {
        return tipoAppDao.getAppVersion()
    }


    override fun getUser(idUser: Int): TipoUsuario {
        return tipoUsuarioDao.getUser(idUser)
    }

    override fun deleteAppVersion() {
        return tipoAppDao.deleteAppVersion()
    }

    override fun insertUser(usuario: TipoUsuario): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoUsuarioDao.insertUser(usuario) }
        return 0
    }

    override fun getUsers(): LiveData<List<TipoUsuario>> {
        return tipoUsuarioDao.getUsers()
    }

    override fun deleteUsuarios() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoUsuarioDao.deleteUsuarios() }
    }


    override fun getLastUser(): TipoUsuario {
        return tipoUsuarioDao.getLastUser()
    }

    override fun getSizeUserList(): Int {
        return tipoUsuarioDao.getSizeUserList()
    }

    ////
    override fun insertTipoTipo(tipoTipo: TipoTipo): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoTipoDao.insertTipoTipo(tipoTipo) }
        return 0
    }

    override fun getTipoList(): LiveData<List<TipoTipo>> {
        return tipoTipoDao.getTipoList()
    }


    override fun deleteTipoTipo() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoTipoDao.deleteTipoTipo() }
    }

    override fun getTipoByCodTipo(idTipoTipo: String, idTipo: String): LiveData<List<TipoTipo>> {
        return tipoTipoDao.getTipoByCodTipo(idTipoTipo, idTipo)
    }


    //
    override fun insertTipoCentroHoy(tipoCentroHoy: TipoCentroHoy): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoCentroHoyDao.insertTipoCentroHoy(tipoCentroHoy) }
        return 0
    }

    override fun getAllCentrosList(): LiveData<List<TipoCentroHoy>> {
        return getAllCentrosList()
    }


    override fun getCentrHoyoNotToday(hoy: Boolean): LiveData<List<TipoCentroHoy>> {
        return tipoCentroHoyDao.getCentrHoyoNotToday(hoy)
    }


    override fun deleteTipoCentrosHoy() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoCentroHoyDao.deleteTipoCentrosHoy() }
    }

    override fun updateCentroHoy(tipoCentroHoy: TipoCentroHoy): Int {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoCentroHoyDao.updateCentroHoy(tipoCentroHoy) }
        return 0
    }

    override fun getCentroById(idCentro: String): TipoCentroHoy {
        return tipoCentroHoyDao.getCentroById(idCentro)
    }

    ////

    override fun insertTipoVisita(tipoVisita: TipoVisita): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoVisitaDao.insertTipoVisita(tipoVisita) }
        return 0
    }

    override fun getVisitaList(): LiveData<List<TipoVisita>> {
        return tipoVisitaDao.getVisitaList()
    }

    override fun deleteTipoVisita() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoVisitaDao.deleteTipoVisita() }
    }

    override fun getVisitaById(idVisita: String): TipoVisita {
        return tipoVisitaDao.getVisitaById(idVisita)
    }

    override fun updateTipoVisita(tipoVisita: TipoVisita): Int {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoVisitaDao.updateTipoVisita(tipoVisita) }
        return 0
    }

    /////

    override fun insertTipoRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute {
            tipoRespuestaPreguntaDao.insertTipoRespuestaPregunta(
                tipoRespuestaPregunta
            )
        }
        return 0
    }

    override fun getRespuestaPreguntaList(): LiveData<List<TipoRespuestaPregunta>> {
        return tipoRespuestaPreguntaDao.getRespuestaPreguntaList()
    }

    override fun deleteRespuestaPregunta() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { tipoRespuestaPreguntaDao.deleteRespuestaPregunta() }
    }

    override fun getRespuestaPreguntaById(idRespuestaPregunta: String): TipoRespuestaPregunta {
        return tipoRespuestaPreguntaDao.getRespuestaPreguntaById(idRespuestaPregunta)
    }

    override fun updateRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Int {
        AsyncTask.THREAD_POOL_EXECUTOR.execute {
            tipoRespuestaPreguntaDao.updateCRespuestaPregunta(
                tipoRespuestaPregunta
            )
        }
        return 0
    }


    /////////

    override fun insertReporteHoraNomina(reporteHoraNomina: TipoReporteHoraNomina): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute {
            this.tipoReporteHoraNominaDao.insertReporteHoraNomina(
                reporteHoraNomina
            )
        }
        return 0
    }

    override fun getReporteHoraNominaList(): LiveData<List<TipoReporteHoraNomina>> {
        return this.tipoReporteHoraNominaDao.getReporteHoraNominaList()
    }

    override fun deleteReporteHoraNomina() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoReporteHoraNominaDao.deleteReporteHoraNomina() }
    }

    /////


    override fun insertHoraNomina(HoraNomina: TipoHoraNomina): Long {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoHoraNominaDao.insertHoraNomina(HoraNomina) }
        return 0
    }

    override fun getHoraNominaList(): LiveData<List<TipoHoraNomina>> {
        return this.tipoHoraNominaDao.getHoraNominaList()
    }

    override fun deleteHoraNomina() {
        AsyncTask.THREAD_POOL_EXECUTOR.execute { this.tipoHoraNominaDao.deleteHoraNomina() }
    }

    override fun getHoraNominaById(idHoraNomina: String): List<TipoHoraNomina> {
        return this.tipoHoraNominaDao.getHoraNominaById(idHoraNomina)
    }


}