package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.rpv.carrefour.tipos.TipoCentroHoy
import com.rpv.carrefour.tipos.TipoTipo
import com.rpv.carrefour.tipos.TipoVisita


@Dao
interface TipoVisitaDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTipoVisita(tipoVisita: TipoVisita): Long

    @Query("SELECT * FROM TipoVisita")
    fun getVisitaList(): LiveData<List<TipoVisita>>

    @Query("DELETE FROM TipoVisita")
    fun deleteTipoVisita()

    @Query("SELECT * FROM TipoVisita WHERE idVisita = :idVisita")
    fun getVisitaById(idVisita: String): TipoVisita

    @Update
    fun updateTipoVisita(tipoVisita: TipoVisita): Int
}