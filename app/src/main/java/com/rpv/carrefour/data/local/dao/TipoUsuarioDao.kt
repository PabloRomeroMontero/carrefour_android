package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.tipos.TipoUsuario

@Dao
 interface TipoUsuarioDao {
    //TipoUsuario
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUser(usuario: TipoUsuario): Long

    @Query("SELECT * FROM TipoUsuario")
    fun getUsers(): LiveData<List<TipoUsuario>>

   @Query("SELECT * FROM TipoUsuario")
   fun getLastUser(): TipoUsuario

    @Query("SELECT * FROM TipoUsuario WHERE IdUsuario = :idUser")
    fun getUser(idUser: Int): TipoUsuario

   @Query("SELECT COUNT(*) FROM TipoUsuario")
   fun getSizeUserList(): Int

    @Query("DELETE FROM TipoUsuario")
    fun deleteUsuarios()
}