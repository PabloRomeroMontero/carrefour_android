package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject

@Entity
data class TipoApp(
    @PrimaryKey var version : Int,
    var name: String,
    var apk: String,
    var _package: String,
    var status: Int
) {



    override fun toString(): String {
        return "TipoApp(name='$name', apk='$apk', version=$version, _package='$_package', status=$status)"
    }

}
