package com.rpv.carrefour.tipos

import com.rpv.carrefour.BuildConfig
import org.json.JSONObject

class TipoPeticion(var peticion: String = "", var data: String = "", var version: Int = BuildConfig.VERSION_CODE) {

    fun toJson() = JSONObject().apply {
        put("peticion", peticion)
        put("data", data)
        put("version", version)
    }.toString()

    override fun toString() = "TipoPeticion(peticion='$peticion', data='$data', version=$version)"

}
