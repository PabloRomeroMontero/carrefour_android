package com.rpv.carrefour.utils

import android.Manifest
import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build

import android.telephony.TelephonyManager
import android.text.Spanned
import android.text.method.DigitsKeyListener
import android.util.Base64
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.rpv.carrefour.data.local.AppDatabase

import java.io.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

fun Context?.log(msg: String) {
    this?.apply {
        F.log(this.javaClass.simpleName, msg)
    }
}



@Suppress("DEPRECATION")
@SuppressLint("HardwareIds")
fun Context?.getImei(): String {
    this?.apply {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> systemService<TelephonyManager>()?.imei
                else -> systemService<TelephonyManager>()?.deviceId
            } ?: ""
        else ""
    }
    return ""
}

fun Context.toUri(file: File): Uri? {

    return when {
        Build.VERSION.SDK_INT < Build.VERSION_CODES.N -> Uri.fromFile(file)
        else -> FileProvider.getUriForFile(this, C.APP.AUTHORITY, file)
    }
}


fun Context?.log(e: Exception) {
    this?.apply {
        F.log(this.javaClass.simpleName, e)
    }
}

fun Fragment.log(msg: String) {
    context.log(msg)
}

fun Fragment.log(e: Exception) {
    context.log(e)
}

fun Context?.dialog(message: String) {
    dialog(message) {}
}

fun Context?.dialog(message: String, okListener: (DialogInterface) -> Unit) {
    this?.apply {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { d, _ -> okListener(d) }
            .create()
            .show()
    }
}

//fun Context?.dialog(message: String, okListener: (DialogInterface) -> Unit) {
//    this?.apply {
//        LottieAlertDialog.Builder(this, DialogTypes.TYPE_LOADING)
//            .setDescription(message)
//            .build()
//            .show()
//    }
//}

fun setColorWindows(activity: Activity) {
    activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    activity.window.statusBarColor =
        ContextCompat.getColor(activity, com.rpv.carrefour.R.color.statusBar)
}

fun Context?.dialog(
    message: String,
    okListener: ((DialogInterface) -> Unit),
    cancelListener: ((DialogInterface) -> Unit)?
) {
    this?.apply {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { d, _ -> okListener(d) }
            .setNegativeButton(android.R.string.cancel) { d, _ -> cancelListener?.invoke(d) }
            .create()
            .show()
    }
}

//fun Context?.dialog(
//    message: String,
//    okListener: ((DialogInterface) -> Unit),
//    cancelListener: ((DialogInterface) -> Unit)?
//) {
//    this?.apply {
//        LottieAlertDialog.Builder(this,DialogTypes.TYPE_LOADING)
//            .setDescription(message)
//            .build()
//            .show()
//    }
//}

fun Fragment.dialog(message: String) {
    context.dialog(message) {}
}

fun Fragment.dialog(message: String, okListener: (DialogInterface) -> Unit) {
    context.dialog(message, okListener)
}

fun Fragment.dialog(
    message: String,
    okListener: ((DialogInterface) -> Unit),
    cancelListener: ((DialogInterface) -> Unit)?
) {
    context.dialog(message, okListener, cancelListener)
}

fun Context?.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    this?.apply {
        Toast.makeText(this, message, duration).show()
    }
}

fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    context.toast(message, duration)
}

inline fun <reified T> Context?.systemService(): T? {
    return this?.let {
        ContextCompat.getSystemService(it, T::class.java)
    }
}


fun Context?.hideKeyboard(view: View?) {
    systemService<InputMethodManager>()?.hideSoftInputFromWindow(
        view?.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun Context?.startActivity(activityClass: Class<out Activity>) {
    this?.apply {
        startActivity(Intent(this, activityClass))
    }
}


//region [Service]

@Suppress("DEPRECATION")
fun Context?.startService(serviceClass: Class<out Service>) {
    if (systemService<ActivityManager>()?.getRunningServices(Integer.MAX_VALUE)
            ?.none { serviceClass.name == it.service.className } == true
    )
        this?.startService(Intent(this, serviceClass))
}

@Suppress("DEPRECATION")
fun Context?.stopService(serviceClass: Class<out Service>) {
    if (systemService<ActivityManager>()?.getRunningServices(Integer.MAX_VALUE)
            ?.any { serviceClass.name == it.service.className } == true
    )
        this?.stopService(Intent(this, serviceClass))
}

//endregion [SERVICE]

//region [File]


fun File.toByteArray(): ByteArray {
    val fis = FileInputStream(this)

    val length = this.length()
    var bytes = ByteArray(0)
    if (length > Integer.MAX_VALUE) {
        return bytes
    }
    bytes = ByteArray(length.toInt())
    var offset = 0
    val numRead: Int = fis.read(bytes, offset, bytes.size - offset)
    while (offset < bytes.size && numRead >= 0) {
        offset += numRead
    }

    if (offset < bytes.size) {
        throw IOException("Could not completely read file " + this.name)
    }

    fis.close()
    return bytes
}

fun File.save(text: String) {

    if (this.create()) {
        try {
            val bos = BufferedWriter(FileWriter(this, true))
            bos.write(text)
            bos.newLine()
            bos.flush()
            bos.close()
        } catch (e: Exception) {
            F.log("File.save", e)
        }
    }
}

fun File.create(): Boolean {

    val root = File(this.parent)
    return if (!root.exists()) {
        root.mkdirs()
    } else {
        true
    }
}

//endregion [File]




fun Spinner.loadSpinners(list: ArrayList<String>, context: Context) {
    // Create an ArrayAdapter using a simple spinner layout and languages array
    val aa = ArrayAdapter(context, R.layout.simple_spinner_item, list)
    // Set layout to use when the list of choices appear
    aa.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
    // Set Adapter to Spinner
    this.adapter = aa
}


object F {


    fun log(tag: String, msg: String) {
//        if (C.APP.DEBUG) {
//            Log.wtf(tag, msg)
//        }
    }

    fun log(tag: String, e: Exception) {
        if (tag.isEmpty())
            log("error", e.toString())
        else
            log(tag, "error: " + e.toString())
    }


    @Suppress("DEPRECATION")
    class InputFilterDecimal(private val before: Int, private val after: Int) :
        DigitsKeyListener(false, true) {
        override fun filter(
            source: CharSequence,
            start: Int,
            end: Int,
            dest: Spanned,
            dStart: Int,
            dEnd: Int
        ): CharSequence? {

            val temp = StringBuilder(dest)
                .insert(dStart, source)
                .toString()

            if (temp == ".") {
                return "0."
            }
            if (temp.indexOf('.') == -1) {
                if (temp.length > before) {
                    return ""
                }
            } else {
                if (temp.substring(
                        0,
                        temp.indexOf('.')
                    ).length > before || temp.substring(
                        temp.indexOf('.') + 1,
                        temp.length
                    ).length > after
                ) {
                    return ""
                }
            }

            return super.filter(source, start, end, dest, dStart, dEnd)
        }
    }

    fun onObsolete(activity: Activity?) {
//        activity?.dialog("La aplicación está desactualizada.\nPulse Aceptar para actualizar.") {
//            logout(activity)
//            activity.startActivity(CheckActivity::class.java)
//            activity.finish()
//        }
    }


    fun logout(context: Context) {
        AppDatabase.getInstance(context).tipoUsuarioDao.deleteUsuarios()
    }

    ///////////////////////////////  DateTime  /////////////////////////

    object DateTime {

        class Date(private val d: Int, private val m: Int, private val y: Int) {

            /** format dd/MM/yyyy **/
            constructor(s: String) : this(
                d = s.split("/")[0].toInt(),
                m = s.split("/")[1].toInt(),
                y = s.split("/")[2].toInt()
            )

            constructor(cal: Calendar) : this(
                d = cal.get(Calendar.DAY_OF_MONTH),
                m = cal.get(Calendar.MONTH) + 1,
                y = cal.get(Calendar.YEAR)
            )

            constructor(millis: Long) : this(
                cal = Calendar.getInstance().apply { timeInMillis = millis })

            override fun toString(): String {
                return "${d.to2Digits()}/${m.to2Digits()}/$y"
            }

        }

        class Time(private val h: Int = 0, private val m: Int = 0) {

            /** format HH:mm **/
            constructor(s: String) : this(h = s.split(":")[0].toInt(), m = s.split(":")[1].toInt())

            constructor(cal: Calendar) : this(
                h = cal.get(Calendar.HOUR_OF_DAY),
                m = cal.get(Calendar.MINUTE)
            )

            constructor(millis: Long) : this(
                cal = Calendar.getInstance().apply { timeInMillis = millis })

            operator fun plus(other: Time): Time {
                return Time(
                    h = (this.h + other.h) + (this.m + other.m) / 60,
                    m = (this.m + other.m) % 60
                )
            }

            operator fun minus(other: Time): Time {

                var mh = this.h - other.h
                var mm = this.m - other.m

                if (mm < 0) {
                    mm += 60
                    mh -= 1
                }

                return Time(h = mh, m = mm)
            }

            override fun toString(): String {
                return "${h.to2Digits()}:${m.to2Digits()}"
            }

        }

        private fun Int.to2Digits(): String {
            return String.format("%02d", this)
        }

    }

    ///////////////////////////////  LOCATION  /////////////////////////


    ///////////////////////////////  VALIDATES  /////////////////////////


    ///////////////////////////////  PHOTOS  ////////////////////////////

    ///////////////////////////////  SECURITY  //////////////////////////

    object Security {

        /**
         * @param value data to encrypt
         * @return String result of encryption
         */
        fun encrypt(value: String, key: String): String {
            val valueBytes = value.toByteArray(charset("UTF-8"))
            val keyBytes = getKeyBytes(key)
            return Base64.encodeToString(encrypt(valueBytes, keyBytes, keyBytes), 0)
        }

        private fun encrypt(
            paramArrayOfByte1: ByteArray,
            paramArrayOfByte2: ByteArray,
            paramArrayOfByte3: ByteArray
        ): ByteArray {
            // setup AES cipher in CBC mode with PKCS #7 padding
            val localCipher = Cipher.getInstance("AES/CBC/PKCS7Padding")//PKCS7Padding

            // encrypt
            localCipher.init(
                Cipher.ENCRYPT_MODE,
                SecretKeySpec(paramArrayOfByte2, "AES"),
                IvParameterSpec(paramArrayOfByte3)
            )
            return localCipher.doFinal(paramArrayOfByte1)
        }

        /**
         * @param value data to decrypt
         * @return String result after decryption
         */
        fun decrypt(value: String, key: String): String {
            val valueBytes = Base64.decode(value, 0)
            val keyBytes = getKeyBytes(key)
            return String(decrypt(valueBytes, keyBytes, keyBytes), charset("UTF-8"))
        }

        private fun decrypt(
            ArrayOfByte1: ByteArray,
            ArrayOfByte2: ByteArray,
            ArrayOfByte3: ByteArray
        ): ByteArray {
            // setup AES cipher in CBC mode with PKCS #7 padding
            val localCipher = Cipher.getInstance("AES/CBC/PKCS7Padding")//PKCS7Padding

            // decrypt
            localCipher.init(
                Cipher.DECRYPT_MODE,
                SecretKeySpec(ArrayOfByte2, "AES"),
                IvParameterSpec(ArrayOfByte3)
            )
            return localCipher.doFinal(ArrayOfByte1)
        }

        private fun getKeyBytes(paramString: String): ByteArray {
            val arrayOfByte1 = ByteArray(16)
            val arrayOfByte2 = paramString.toByteArray(charset("UTF-8"))
            System.arraycopy(
                arrayOfByte2,
                0,
                arrayOfByte1,
                0,
                Math.min(arrayOfByte2.size, arrayOfByte1.size)
            )
            return arrayOfByte1
        }
    }

    ///////////////////////////////  NATIVE  ////////////////////////////


    external fun getMD5(): String

}



// KeyBoard

fun View.hideSoftKeyboard(): Boolean {
    val imm = context.getSystemService(
        Context.INPUT_METHOD_SERVICE
    ) as InputMethodManager
    return imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.openSoftKeyboard(): Boolean {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    return imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}



