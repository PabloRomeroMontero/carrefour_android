package com.rpv.carrefour.tipos

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject

@Entity
class TipoVisita(
    @PrimaryKey var idVisita : Int,
    val idPersona : Int,
    val idUsuario : Int,
    val idCentro : Int,
    val fecha : String,
    val horaEntrada : String,
    val horaRealEntrada : String,
    var horaSalida : String,
    var horaRealSalida : String,
    val deleted : Boolean
) {
    override fun toString(): String {
        return "TipoVisita(idVisita=$idVisita, idPersona='$idPersona', idUsuario='$idUsuario', idCentro='$idCentro', fecha='$fecha', horaEntrada='$horaEntrada', horaRealEntrada='$horaRealEntrada', horaSalida='$horaSalida', horaRealSalida='$horaRealSalida')"
    }
}