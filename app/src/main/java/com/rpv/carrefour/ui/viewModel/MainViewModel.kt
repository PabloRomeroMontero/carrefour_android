package com.rpv.carrefour.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rpv.carrefour.data.local.AppDatabase
import com.rpv.carrefour.data.local.LocalRepositoryImpl
import com.rpv.carrefour.tipos.*
import java.util.*


class MainViewModel(database: AppDatabase) : ViewModel() {
    private val repository = LocalRepositoryImpl(
        database.tipoUsuarioDao,
        database.tipoAppDao,
        database.tipoTipoDao,
        database.tipoCentroDao,
        database.tipoVisitaDao,
        database.tipoRespuestaPreguntaDao,
        database.tipoReporteHoraNominaDao,
        database.tipoHoraNominaDao
    )

    private val listaOpciones: MutableLiveData<MutableList<Int>> = MutableLiveData()

    private var listaPreguntas: MutableLiveData<MutableList<TipoPregunta>> = MutableLiveData()

    var appList: LiveData<List<TipoApp>> = repository.getAppVersion()

    private var listaUsuario: LiveData<List<TipoUsuario>> = repository.getUsers()

    private var listaTipos: LiveData<List<TipoTipo>> = repository.getTipoList()

    private var listaCentrosHoyToday: LiveData<List<TipoCentroHoy>> =
        repository.getCentrHoyoNotToday(true)

    private var listaCentrosHoyNotToday: LiveData<List<TipoCentroHoy>> =
        repository.getCentrHoyoNotToday(false)


    //Modulo de horas nomina
    private var listaTipoHorasNomina: LiveData<List<TipoHoraNomina>> =
        repository.getHoraNominaList()
    private var listaTipoReporteHorasNomina: LiveData<List<TipoReporteHoraNomina>> =
        repository.getReporteHoraNominaList()
    private var listaSeccionesTienda: MutableLiveData<MutableList<String>> = MutableLiveData()


//    var listReporteHorasNomina = ArrayList<TipoReporteHoraNomina>()
//    var listSeccionesNomina = ArrayList<String>()
//    var listReporteHorasNominaOrdenada = ArrayList<ArrayList<TipoReporteHoraNomina>>()


    //region [TipoUsuarios]
    fun insertarUsuario(tipoUsuario: TipoUsuario) {
        repository.insertUser(tipoUsuario)
    }

    fun getUserById(userId: Int): TipoUsuario {
        return repository.getUser(userId)
    }

    fun getUsuariosList(): LiveData<List<TipoUsuario>> {
        return listaUsuario
    }

    fun refreshList() {
//        listaUsuario = repository.getUsers()
    }

    fun deleteUsers() {
        repository.deleteUsuarios()
    }

    fun getLastUser(): TipoUsuario {
        return repository.getLastUser()
    }

    fun userIsEmpty(): Boolean {
        return repository.getSizeUserList() == 0
    }

    //endregion

    //region    [TipoApp]

    fun getTipoApp(): LiveData<List<TipoApp>> {
        return repository.getAppVersion()
    }

    fun insertTipoApp(tipoApp: TipoApp) {
        repository.insertAppVersion(tipoApp)
    }

    //endregion

    //region [TipoTipo]
    fun getTipoList(): LiveData<List<TipoTipo>> {
        return listaTipos
    }


    fun deleteTipos() {
        repository.deleteTipoTipo()
    }

    fun insterTipo(tipoTipo: TipoTipo) {
        repository.insertTipoTipo(tipoTipo)
    }
    //endregion

    //region    [TipoCentro]

    fun insertTipoCentroHoy(tipoCentroHoy: TipoCentroHoy): Long {
        return repository.insertTipoCentroHoy(tipoCentroHoy)
    }

    fun getAllCentrosList(): LiveData<List<TipoCentroHoy>> {
        return getAllCentrosList()
    }

    fun getCentrHoyoNotToday(hoy: Boolean): LiveData<List<TipoCentroHoy>> {
        return repository.getCentrHoyoNotToday(hoy)
    }

    fun getCentroHoyToday(): LiveData<List<TipoCentroHoy>> {
        return listaCentrosHoyToday
    }

    fun getCentroHoyNotToday(): LiveData<List<TipoCentroHoy>> {
        return listaCentrosHoyNotToday
    }

    fun deleteTipoCentrosHoy() {
        repository.deleteTipoCentrosHoy()
    }

    fun updateCenterHoy(tipoCentroHoy: TipoCentroHoy) {
        repository.updateCentroHoy(tipoCentroHoy)
    }

    fun getCentroById(idCentro: String): TipoCentroHoy {
        return repository.getCentroById(idCentro)
    }

    fun getCenterByFilter(filtro: String): List<TipoCentroHoy> {
        val filtroArray = filtro.split(" ")
        val listaAux: MutableList<TipoCentroHoy> = mutableListOf()

        return if (filtro == "") {
            listaCentrosHoyNotToday.value!!
        } else {
            for (i in filtroArray.indices) {
                if (i == 0)
                    listaAux.replaceList(filterAux(filtroArray[i], listaCentrosHoyNotToday.value!!))
                else
                    listaAux.replaceList(filterAux(filtroArray[i], listaAux))
            }
            listaAux
        }
    }


    fun filterAux(filtro: String, value: List<TipoCentroHoy>): List<TipoCentroHoy> {
        return value.filter {
            it.nombre.toLowerCase(Locale.ROOT).startsWith(filtro.toLowerCase(Locale.ROOT)) or
                    it.domicilio.toLowerCase(Locale.ROOT)
                        .startsWith(filtro.toLowerCase(Locale.ROOT)) or
                    it.idCentro.toString().startsWith(filtro) or
                    it.provincia.toLowerCase(Locale.ROOT)
                        .startsWith(filtro.toLowerCase(Locale.ROOT)) or
                    it.municipio.toLowerCase(Locale.ROOT)
                        .startsWith(filtro.toLowerCase(Locale.ROOT))
        }
    }

    //endregion

    //region [ListaOpciones]
    fun getListaOpciones(): MutableLiveData<MutableList<Int>> {
        return listaOpciones
    }

    fun addlistaOpciones(lista: String) {
        val listaAux = mutableListOf<Int>()
        for (i in lista.substring(1, lista.length - 1).split(',').toTypedArray()) {
            listaAux.add(i.toInt())
        }
        if (listaOpciones.value != null) {
            listaOpciones.value!!.clear()
        }
        listaOpciones.value = listaAux
    }

    //endregion

    //region [ListaPreguntas]
    fun getListaPreguntas(): MutableLiveData<MutableList<TipoPregunta>> {
        return listaPreguntas
    }

    fun addPreguntas(tipoPregunta: List<TipoPregunta>) {
        val listaAux = mutableListOf<TipoPregunta>()
        for (i in tipoPregunta)
            listaAux.add(i)

        listaPreguntas.value = listaAux
    }

    fun clearListaPreguntas() {
        if(listaPreguntas.value != null)
            listaPreguntas.value!!.clear()
    }


    //endregion

    //region [Visitas]
    fun insertTipoVisita(tipoVisita: TipoVisita): Long {
        return repository.insertTipoVisita(tipoVisita)
    }

    fun getVisitaList(): LiveData<List<TipoVisita>> {
        return repository.getVisitaList()
    }

    fun deleteTipoVisita() {
        repository.deleteTipoVisita()
    }

    fun getVisitaById(idVisita: String): TipoVisita {
        return repository.getVisitaById(idVisita)
    }

    fun updateTipoVisita(tipovisita: TipoVisita) {
        repository.updateTipoVisita(tipovisita)
    }

    //endregion


    //region [PreguntaRespuestas]
    fun insertTipoRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Long {
        return repository.insertTipoRespuestaPregunta(tipoRespuestaPregunta)
    }

    fun getTipoRespuestaPreguntaList(): LiveData<List<TipoRespuestaPregunta>> {
        return repository.getRespuestaPreguntaList()
    }

    fun deleteTipoRespuestaPregunta() {
        repository.deleteRespuestaPregunta()
    }

    fun getRespuestaPreguntaById(idRespuesta: String): TipoRespuestaPregunta {
        return repository.getRespuestaPreguntaById(idRespuesta)
    }

    fun updateRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta) {
        repository.updateRespuestaPregunta(tipoRespuestaPregunta)
    }

    //endregion

    //region    [HorasNomina]
    fun insertHoraNomina(horaNomina: TipoHoraNomina): Long {
        return repository.insertHoraNomina(horaNomina)
    }

    fun getHoraNominaList(): LiveData<List<TipoHoraNomina>> {
        return listaTipoHorasNomina
    }

    fun deleteHoraNomina() {
        return repository.deleteHoraNomina()
    }


    //endregion


    //region    [ReporteHorasNominas]

    fun insertReporteHoraNomina(reporteHoraNomina: TipoReporteHoraNomina): Long {
        return repository.insertReporteHoraNomina(reporteHoraNomina)
    }

    fun getReporteHoraNominaList(): LiveData<List<TipoReporteHoraNomina>> {
        return listaTipoReporteHorasNomina
    }

    fun deleteReporteHoraNomina() {
        repository.deleteReporteHoraNomina()
    }


    fun getHoraNominaById(idHoraNomina: String): List<TipoHoraNomina> {
        return repository.getHoraNominaById(idHoraNomina)
    }
    //endregion


    //region [SeccionTienda]
    fun getListaSeccion(): MutableLiveData<MutableList<String>> {
        return listaSeccionesTienda
    }

    fun addSecciones(secciones: List<String>) {
        val listaAux = mutableListOf<String>()
        for (i in secciones)
            listaAux.add(i)

        listaSeccionesTienda.value = listaAux
    }

    fun clearListaSecciones() {
        if (listaSeccionesTienda.value != null) {
            listaSeccionesTienda.value!!.clear()
        }
    }
    //endregion
}


private fun <E> MutableList<E>.replaceList(filterAux: List<E>) {
    this.clear()
    for (i in filterAux) {
        this.add(i)
    }
}




