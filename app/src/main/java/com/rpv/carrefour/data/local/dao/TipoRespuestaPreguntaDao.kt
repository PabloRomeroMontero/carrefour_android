package com.rpv.carrefour.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.rpv.carrefour.tipos.TipoCentroHoy
import com.rpv.carrefour.tipos.TipoRespuestaPregunta

@Dao
interface TipoRespuestaPreguntaDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertTipoRespuestaPregunta(tipoTipo: TipoRespuestaPregunta): Long

    @Query("SELECT * FROM TipoRespuestaPregunta")
    fun getRespuestaPreguntaList(): LiveData<List<TipoRespuestaPregunta>>


    @Query("DELETE FROM TipoRespuestaPregunta")
    fun deleteRespuestaPregunta()

    @Query("SELECT * FROM TipoRespuestaPregunta WHERE idRespuesta = :idRespuestaPregunta")
    fun getRespuestaPreguntaById(idRespuestaPregunta: String): TipoRespuestaPregunta

    @Update
    fun updateCRespuestaPregunta(tipoRespuestaPregunta: TipoRespuestaPregunta): Int

}