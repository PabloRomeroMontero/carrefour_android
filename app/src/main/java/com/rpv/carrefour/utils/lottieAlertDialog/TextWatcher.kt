package com.rpv.carrefour.utils.lottieAlertDialog

interface TextWatcher {
    fun onTextChanged(dialog: LottieAlertDialogSearchCenter,s: CharSequence?, start: Int, before: Int, count: Int)
}